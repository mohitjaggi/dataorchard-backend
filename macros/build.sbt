/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

name := """datagears-macros"""

version := "0.0.1"

scalaVersion := "2.11.8"

libraryDependencies += "org.clapper" %% "classutil" % "1.0.13"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.9"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

