/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag.nodetypes

import play.api.libs.json.{JsValue, Json}

case class WordCounter(l: String, p: WordCounter.Params) extends NodeWithParams(l, p) {
  override def sparkCode(predecessors: Set[Node]): String = {
    require(predecessors.size == 1, "WordCounter needs exactly one input")
    val predecessor = predecessors.head
    s"""
       |val ${label}_tmp =  ${predecessor.label}.explode("line","word")((line: String) => line.split(\"${params.delimiter.value}\")).drop("line")
       |val $label = words.groupBy("word").count
     """.stripMargin
  }
}

object WordCounter extends NodeMeta("WordCounter",
  "Counts words",
  NodeMeta.BATCH_AGGREGATOR,
  NodeSchemaSource.Fixed,
  Some(Json.obj("word" -> "string", "count" -> "long")
    )) {

  case class Params(delimiter: CommonParams.WordDelimiter)

  def getOutputSchema(json: JsValue): String = s"word: String, count: Long"
}

case class CsvFileWriter(l: String, p: CsvFileWriter.Params) extends NodeWithParams(l, p) {
  override def sparkCode(predecessors: Set[Node]): String = {
    require(predecessors.size == 1, "CsvFileWriter needs exactly one input")
    val predecessor = predecessors.head
    s"""
       |${predecessor.label}.coalesce(1).write.csv(\"${params.fileName.value}\")
     """.stripMargin
  }
}

object CsvFileWriter extends NodeMeta("CsvFileWriter",
  "Writes a CSV file",
  NodeMeta.BATCH_SINK,
  NodeSchemaSource.Calculated) {

  case class Params(fileName: CommonParams.FileName,
                    delimiter: CommonParams.FieldDelimiter,
                    writeHeader: CommonParams.WriteHeader,
                    coalesce: CommonParams.SinglePart)

  def getOutputSchema(json: JsValue): String = s"same as input"
}
