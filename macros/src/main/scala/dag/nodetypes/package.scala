/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag

package object nodetypes {
  val nodeNames: List[String] = List(
    "dag.nodetypes.TextFileReader",
    "dag.nodetypes.WordCounter",
    "dag.nodetypes.CsvFileWriter",
    "dag.nodetypes.JdbcReader",
    "dag.nodetypes.ParquetReader",
    "dag.nodetypes.ColumnSelector")
}
