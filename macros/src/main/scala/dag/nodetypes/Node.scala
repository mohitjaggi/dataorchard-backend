/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag.nodetypes

import play.api.libs.json.JsObject

import scala.reflect.runtime.{universe => ru}

/**
  * Ultimate parent of any type of Node
  */
trait Node {
  def label: String

  def sparkCode(predecessor: Set[Node]): String
}

/**
  * A node that has parameters, e.g. TextFileReader needs a fileName param
  *
  * @param l      label for the node, must be unique within a DAG
  * @param params params of the node e.g. fileName for TextFileReader
  * @tparam P type of the params of the node e.g. String is the type of fileName for TextFileReader
  */
abstract class NodeWithParams[P](l: String, val params: P) extends Node {
  require(l.matches("""^[_a-zA-Z][a-zA-Z0-9_]*$"""),
    "Label should contain letters, numbers and underscores only and start with an underscore or letter ")
  override val label = l
}

object NodeSchemaSource {
  sealed trait EnumVal

  /**
    * User enters the schema e.g. most batch or stream sources
    */
  case object User extends EnumVal

  /**
    * Fixed schema does not depend on input e.g. TextFileReader
    */
  case object Fixed extends EnumVal

  /**
    * Node provides a method to determine its output schema based on input schema and other node params e.g. WordCounter
    */
  case object Calculated extends EnumVal
}

abstract class NodeMeta(val name: String,
                        val description: String,
                        val tpe: String,
                        val schemaSource: NodeSchemaSource.EnumVal,
                        val fixedSchema: Option[JsObject] = None)

object NodeMeta {
  val BATCH_SOURCE = "batchsource"
  val BATCH_AGGREGATOR = "batchaggregator"
  val BATCH_SINK = "batchsink"
  val BATCH_TRANSFORM = "transform"
}

/**
  * Configuration of a node parameter. Has constant values, hence extended by singleton objects only
  *
  * @param name        display name of the parameter
  * @param description description of the parameter
  * @param required    is it mandatory for user to specify
  * @param default     the default value of the parameter, only meaningful if required is false
  * @tparam T the type of the parameter e.g. String, Boolean etc
  */
abstract class NodeParamConfig[T: ru.TypeTag](val name: String,
                                              val description: String,
                                              val required: Boolean,
                                              val default: T) {

  import play.api.libs.json.Json

  val typeString: String = {
    val tpe = ru.typeOf[T]
    if (tpe =:= ru.typeOf[String])
      "string"
    else if (tpe =:= ru.typeOf[Boolean])
      "boolean"
    else if (tpe =:= ru.typeOf[Integer])
      "integer"
    else if (tpe =:= ru.typeOf[Seq[String]])
      "seqOfString"
    else
      throw new RuntimeException("bad type")
  }

  val defaultJsonString: String = {
    val tpe = ru.typeOf[T]
    if (tpe =:= ru.typeOf[String])
      s""""$default""""
    else if (tpe =:= ru.typeOf[Boolean])
      s"$default"
    else if (tpe =:= ru.typeOf[Integer])
      s"$default"
    else if (tpe =:= ru.typeOf[Seq[String]])
      Json.toJson(default.asInstanceOf[Seq[String]].toArray).toString()
    else
      throw new RuntimeException("bad type")
  }

}

/**
  * A parameter of a node
  *
  * @param value  the value of the parameter
  * @param config the constant configuration of the parameter
  * @tparam T the type of the parameter e.g. String, Boolean etc
  */
abstract class NodeParam[T](val value: T,
                            val config: NodeParamConfig[T])

/**
  * Commonly used parameter types
  */
object CommonParams {

  object FileNameConfig extends NodeParamConfig[String]("fileName",
    "name of the input file",
    true,
    "")

  /**
    * A file name including path. Required.
    *
    * @param v path or URI
    */
  case class FileName(v: String) extends NodeParam[String](v, FileNameConfig)

  object WordDelimiterConfig extends NodeParamConfig[String]("delimiter",
    "delimiter for splitting words",
    false,
    " ")

  /**
    * Character that separates words in text. Required.
    *
    * @param v String containing a single character
    */
  case class WordDelimiter(v: String) extends NodeParam[String](v, WordDelimiterConfig)

  object FieldDelimiterConfig extends NodeParamConfig[String]("delimiter",
    "delimiter between fields",
    false,
    " ")

  /**
    * Character that separates fields in CSV. Required.
    *
    * @param v String containing a single character
    */
  case class FieldDelimiter(v: String) extends NodeParam[String](v, FieldDelimiterConfig)

  object DeleteOnSuccessConfig extends NodeParamConfig[Boolean]("deleteOnSuccess",
    "delete input file on success",
    false,
    false)

  /**
    * Whether to delete the input when pipeline is successful. Optional. Defaults to false.
    *
    * @param v true or false
    */
  case class DeleteOnSuccess(v: Boolean) extends NodeParam[Boolean](v, DeleteOnSuccessConfig)

  object WriteHeaderConfig extends NodeParamConfig[Boolean]("writeHeader",
    "include a header in output",
    false,
    true)

  /**
    * Whether to include a header in output. Optional. Defaults to true.
    *
    * @param v true or false
    */
  case class WriteHeader(v: Boolean) extends NodeParam[Boolean](v, WriteHeaderConfig)

  object SinglePartConfig extends NodeParamConfig[Boolean]("singlePart",
    "combine output into a single partition",
    false,
    false)

  /**
    * Whether to include a header in output. Optional. Defaults to true.
    *
    * @param v true or false
    */
  case class SinglePart(v: Boolean) extends NodeParam[Boolean](v, SinglePartConfig)

  object JdbcUrlConfig extends NodeParamConfig[String]("jdbcUrl",
    "URL of JDBC source in the form jdbc:subprotocol:subname",
    true,
    "")

  /**
    * The URL of the JDBC source. Required.
    *
    * @param v URL of JDBC source in the form jdbc:subprotocol:subname
    */
  case class JdbcUrl(v: String) extends NodeParam[String](v, JdbcUrlConfig)

  object JdbcTableNameConfig extends NodeParamConfig[String]("tableName",
    "Name of the JDBC source table",
    true,
    "")

  /**
    * Name of the table in the JDBC source
    *
    * @param v table name
    */
  case class JdbcTableName(v: String) extends NodeParam[String](v, JdbcTableNameConfig)

  object UserNameConfig extends NodeParamConfig[String]("userName",
    "user name for accessing the resource",
    true,
    "")

  /**
    * User name for accessing the resource
    *
    * @param v user name
    */
  case class UserName(v: String) extends NodeParam[String](v, UserNameConfig)

  object PasswordConfig extends NodeParamConfig[String]("password",
    "password for accessing the resource",
    true,
    "")

  /**
    * Sequence of column names
    *
    * @param v password
    */
  case class Password(v: String) extends NodeParam[String](v, PasswordConfig)

  object ColumnNames extends NodeParamConfig[Seq[String]]("columnNames",
    "sequence of column names",
    true,
    List(""))

  case class ColumnNames(v: Seq[String]) extends NodeParam[Seq[String]](v, ColumnNames)

}


