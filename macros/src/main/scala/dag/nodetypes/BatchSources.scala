/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag.nodetypes

import play.api.libs.json.{JsValue, Json}

case class TextFileReader(l: String, p: TextFileReader.Params) extends NodeWithParams(l, p) {
  override def sparkCode(predecessor: Set[Node]): String =
    s"""
       |val $label = spark.sparkContext.textFile(\"${params.fileName.value}\").toDF("line")
     """.stripMargin
}

object TextFileReader extends NodeMeta("TextFileReader",
  "Reads a text file",
  NodeMeta.BATCH_SOURCE,
  NodeSchemaSource.Fixed,
  Some(Json.obj("line" -> "string"))) {

  case class Params(fileName: CommonParams.FileName,
                    deleteOnSuccess: CommonParams.DeleteOnSuccess)

  def getOutputSchema(json: JsValue): String = s"line : String"

}

case class JdbcReader(l: String, p: JdbcReader.Params) extends NodeWithParams(l, p) {
  override def sparkCode(predecessor: Set[Node]): String =
    s"""
       |//FIXME
     """.stripMargin
}

object JdbcReader extends NodeMeta("JdbcReader",
  "Read from a JDBC source",
  NodeMeta.BATCH_SOURCE,
  NodeSchemaSource.User) {

  case class Params(url: CommonParams.JdbcUrl,
                    table: CommonParams.JdbcTableName,
                    username: CommonParams.UserName,
                    password: CommonParams.Password)

  def getOutputSchema(json: JsValue): String = s"field1: Int ..."
}

case class ParquetReader(l: String, p: ParquetReader.Params) extends NodeWithParams(l, p) {
  override def sparkCode(predessor: Set[Node]): String =
    s"""
       |val $label = spark.sparkContext.parquet(\"${params.fileName.value}\")
     """.stripMargin
}

object ParquetReader extends NodeMeta("ParquetReader",
  "Reads a parquet file",
  NodeMeta.BATCH_SOURCE,
  NodeSchemaSource.User) {

  case class Params(fileName: CommonParams.FileName,
                    deleteOnSuccess: CommonParams.DeleteOnSuccess)

  def getOutputSchema(json: JsValue): String = s"field1: Int ..."
}
