/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag.nodetypes

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

object JsonReaders {
  val TextFileReaderParamsReads: Reads[TextFileReader.Params] = (
    (JsPath \ "fileName").read[String] and
      (JsPath \ "deleteOnSuccess").read[Boolean]
    ) { (n: String, d: Boolean) => TextFileReader.Params(CommonParams.FileName(n), CommonParams.DeleteOnSuccess(d)) }

  val TextFileReader_reads: Reads[TextFileReader] = (
    (JsPath \ "label").read[String] and
      (JsPath \ "properties").read[TextFileReader.Params](TextFileReaderParamsReads)
    ) (TextFileReader.apply _)

  val WordCounterParamsReads: Reads[WordCounter.Params] =
    (JsPath \ "delimiter").read[String].map { (d: String) => WordCounter.Params(CommonParams.WordDelimiter(d)) }

  val WordCounter_reads: Reads[WordCounter] = (
    (JsPath \ "label").read[String] and
      (JsPath \ "properties").read[WordCounter.Params](WordCounterParamsReads)
    ) (WordCounter.apply _)

  val JdbcReaderParamsReads: Reads[JdbcReader.Params] = (
    (JsPath \ "jdbcUrl").read[String] and
      (JsPath \ "tableName").read[String] and
      (JsPath \ "userName").read[String] and
      (JsPath \ "password").read[String]
    ) { (u: String, t: String, un: String, p: String) => JdbcReader.Params(
    CommonParams.JdbcUrl(u),
    CommonParams.JdbcTableName(t),
    CommonParams.UserName(un),
    CommonParams.Password(p))
  }

  val JdbcReader_reads: Reads[JdbcReader] = (
    (JsPath \ "label").read[String] and
      (JsPath \ "properties").read[JdbcReader.Params](JdbcReaderParamsReads)
    ) (JdbcReader.apply _)

  val ParquetReaderParamsReads: Reads[ParquetReader.Params] = (
    (JsPath \ "fileName").read[String] and
      (JsPath \ "deleteOnSuccess").read[Boolean]
    ) { (n: String, d: Boolean) => ParquetReader.Params(CommonParams.FileName(n), CommonParams.DeleteOnSuccess(d)) }

  val ParquetReader_reads: Reads[ParquetReader] = (
    (JsPath \ "label").read[String] and
      (JsPath \ "properties").read[ParquetReader.Params](ParquetReaderParamsReads)
    ) (ParquetReader.apply _)

  val CsvFileWriterParamsReads: Reads[CsvFileWriter.Params] = (
    (JsPath \ "fileName").read[String] and
      (JsPath \ "delimiter").read[String] and
      (JsPath \ "writeHeader").read[Boolean] and
      (JsPath \ "singlePart").read[Boolean]
    ) { (n: String, d: String, h: Boolean, s: Boolean) => CsvFileWriter.Params(CommonParams.FileName(n),
    CommonParams.FieldDelimiter(d),
    CommonParams.WriteHeader(h),
    CommonParams.SinglePart(s))
  }

  val CsvFileWriter_reads: Reads[CsvFileWriter] = (
    (JsPath \ "label").read[String] and
      (JsPath \ "properties").read[CsvFileWriter.Params](CsvFileWriterParamsReads)
    ) (CsvFileWriter.apply _)

  val ColumnSelectorParamsReads: Reads[ColumnSelector.Params] = (__ \ "columnNames").read[Seq[String]]
    .map(CommonParams.ColumnNames(_))
    .map(ColumnSelector.Params)

  val ColumnSelector_reads: Reads[ColumnSelector] = (
    (JsPath \ "label").read[String] and
      (JsPath \ "properties").read[ColumnSelector.Params](ColumnSelectorParamsReads)
    ) (ColumnSelector.apply _)
}
