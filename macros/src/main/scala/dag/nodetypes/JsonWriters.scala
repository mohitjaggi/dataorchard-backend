/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag.nodetypes

import play.api.libs.json._
import play.api.libs.functional.syntax._

object JsonWriters {
  val textFileReaderParamWrites = new Writes[TextFileReader.Params] {
    def writes(p: TextFileReader.Params) = Json.obj(
      "deleteOnSuccess" -> p.deleteOnSuccess.v,
      "fileName" -> p.fileName.v
    )
  }
  implicit val textFileReaderWrites = new Writes[TextFileReader] {
    def writes(n: TextFileReader) = Json.obj(
      "name" -> "dag.nodetypes.TextFileReader",
      "label" -> n.l,
      "properties" -> Json.toJson(n.params)(textFileReaderParamWrites)
    )
  }
  val jdbcReaderParamWrites = new Writes[JdbcReader.Params] {
    def writes(p: JdbcReader.Params) = Json.obj(
      "jdbcUrl" -> p.url.v,
      "tableName" -> p.table.v,
      "userName" -> p.username.v,
      "password" -> p.password.v
    )
  }
  implicit val jdbcReaderWrites = new Writes[JdbcReader] {
    def writes(n: JdbcReader) = Json.obj(
      "name" -> "dag.nodetypes.JdbcReader",
      "label" -> n.l,
      "properties" -> Json.toJson(n.params)(jdbcReaderParamWrites)
    )
  }
  val parquetReaderParamWrites = new Writes[ParquetReader.Params] {
    def writes(p: ParquetReader.Params) = Json.obj(
      "deleteOnSuccess" -> p.deleteOnSuccess.v,
      "fileName" -> p.fileName.v
    )
  }
  implicit val parquetReaderWrites = new Writes[ParquetReader] {
    def writes(n: ParquetReader) = Json.obj(
      "name" -> "dag.nodetypes.ParquetReader",
      "label" -> n.l,
      "properties" -> Json.toJson(n.params)(parquetReaderParamWrites)
    )
  }
  val csvFileWriterParamWrites = new Writes[CsvFileWriter.Params] {
    def writes(p: CsvFileWriter.Params) = Json.obj(
      "fileName" -> p.fileName.v,
      "delimiter" -> p.delimiter.v,
      "writeHeader" -> p.writeHeader.v,
      "singlePart" -> p.coalesce.v
    )
  }
  implicit val csvFileWriterWrites = new Writes[CsvFileWriter] {
    def writes(n: CsvFileWriter) = Json.obj(
      "name" -> "dag.nodetypes.CsvFileWriter",
      "label" -> n.l,
      "properties" -> Json.toJson(n.params)(csvFileWriterParamWrites)
    )
  }

  val columnSelectorParamWrites: Writes[ColumnSelector.Params] = (__ \ "columnNames").write[Seq[String]].contramap {
    (c : ColumnSelector.Params) => c.colNames.v
  }
  implicit val columnSelectorWrites = new Writes[ColumnSelector] {
    def writes(n: ColumnSelector) = Json.obj(
      "name" -> "dag.nodetypes.ColumnSelector",
      "label" -> n.l,
      "properties" -> Json.toJson(n.params)(columnSelectorParamWrites)
    )
  }

}
