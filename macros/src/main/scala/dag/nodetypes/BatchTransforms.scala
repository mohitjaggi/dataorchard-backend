/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag.nodetypes

import play.api.libs.json.JsValue

case class ColumnSelector(l: String, p: ColumnSelector.Params) extends NodeWithParams(l, p) {
  override def sparkCode(predecessors: Set[Node]): String = {
    val predecessor = predecessors.head

    s"""
       |val columnNamesFor_$label = $p.colNames.value
       |val $label = ${predecessor.label}.select(columnNamesFor_$label.map(c => col(c)): _*)
     """.stripMargin
  }
}

object ColumnSelector extends NodeMeta("ColumnSelector",
  "Selects specified columns",
  NodeMeta.BATCH_TRANSFORM,
  NodeSchemaSource.Calculated) {

  case class Params(colNames: CommonParams.ColumnNames)

  def getOutputSchema(input: JsValue): String = {
    println(input)
    s"line : String"
  }

}
