/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */
package macros

import dag.nodetypes.Node
import org.clapper.classutil.ClassFinder
import play.api.libs.json._

import scala.language.experimental.macros

object NodeTypeMacros {

  import scala.reflect.macros.blackbox

  /*
      get all classes that are "Node"s
   */
  val finder = ClassFinder()
  val classes = finder.getClasses
  val nodeClasses = ClassFinder.concreteSubclasses("dag.nodetypes.Node", classes)
  nodeClasses.foreach(println)
  println(nodeClasses.count(_ == true))

  def parse(jsonTree: JsValue): Node = macro parserImpl

  def parserImpl(c: blackbox.Context)(jsonTree: c.Tree) = {
    import c.universe._
    val q"$json" = jsonTree
    val cases = dag.nodetypes.nodeNames.map { nodeClassName =>
      val nodeClassShortName = nodeClassName.split('.').last
      val reader = c.parse(s"dag.nodetypes.JsonReaders.${nodeClassShortName}_reads")
      val caseStmt = cq"""$nodeClassName => (($json).validate($reader)).get.asInstanceOf[dag.nodetypes.Node]"""
    //  println(showCode(caseStmt))
      caseStmt
    }

    val matchCaseStmt =
      q"""
       import play.api.libs.json._
       ($json \ "name").as[String] match { case ..$cases }
      """
   // println(showCode(matchCaseStmt))
    matchCaseStmt
  }

  def getOutputSchema(nodeTypeNameTree: String, jsonTree: JsValue): String = macro getOutputSchemaImpl

  def getOutputSchemaImpl(c: blackbox.Context)(nodeTypeNameTree: c.Tree, jsonTree: c.Tree) = {
    import c.universe._
    val q"$nodeTypeName" = nodeTypeNameTree
    val q"$json" = jsonTree

    val cases = dag.nodetypes.nodeNames.map { nodeClassName =>
      val nodeClass = c.parse(s"${nodeClassName}.getOutputSchema($json)")
      val caseStmt = cq"""$nodeClassName => $nodeClass"""
     // println(showCode(caseStmt))
      caseStmt
    }

    val matchCaseStmt =
      q"""
       $nodeTypeName match { case ..$cases }
      """
    //println(showCode(matchCaseStmt))
    matchCaseStmt

  }

}