/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package forms.core

import play.api.data.Forms._
import play.api.data._

/**
  * The `Schema` form.
  */
object SchemaForm {

  // FIXME: show the fields as repeated inputs in form, but that requires javascript
//  val form = Form(mapping(
//    "Name" -> nonEmptyText,
//    "Description" -> text,
//    "Fields" -> seq(mapping("Field Name" -> nonEmptyText,
//      "Field Type" -> nonEmptyText)
//    (Field.apply)(Field.unapply)))
//  (Schema.apply)(Schema.unapply)
//  )

  val form = Form(mapping(
    "Name" -> nonEmptyText,
    "Description" -> text,
    "Fields" -> nonEmptyText)
  (Schema.apply)(Schema.unapply2)
  )

  case class Field(name: String, tpe: String)

  case class Schema(name: String,
                    description: String,
                    fields: Seq[Field]
                   )

  object Schema {
    def apply(name: String, description: String, fields: String) = {
      val fs = fields.split("[\n\r]+").map { field =>
        val f = field.split("[\\t ]+")
        Field(f(0).trim, f(1).trim)
      }
      new Schema(name, description, fs)
    }

    def unapply2(arg: Schema): Option[(String, String, String)] = {
      val fieldsStr = arg.fields.map { f => s"$f.name, $f.tpe"}
        .mkString("\n")

     Some(arg.name, arg.description, fieldsStr)
    }
  }

}
