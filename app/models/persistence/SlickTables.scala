/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.persistence

import models.entities._
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import slick.driver.JdbcProfile

/**
  * The companion object.
  */
object SlickTables extends HasDatabaseConfig[JdbcProfile] {

  protected lazy val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._
  //import com.github.tototoshi.slick.H2JodaSupport._ //FIXME: change to postgres

  abstract class BaseTable[T](tag: Tag, name: String) extends Table[T](tag, name) {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  }

  class UsersTable(tag: Tag) extends Table[User](tag, "users") {
    def userId = column[String]("userId")

    def providerId = column[String]("providerId")

    def providerKey = column[String]("providerKey")

    def firstName = column[String]("firstName")

    def lastName = column[String]("lastName")

    def email = column[String]("email")

    def avatarURL = column[Option[String]]("avatarURL")

    def activated = column[Boolean]("activated")

    def * = (userId, providerId, providerKey, firstName, lastName, email, avatarURL, activated) <> ((User.apply _).tupled, User.unapply _)
  }

  val usersTable = TableQuery[UsersTable]

  class GroupsTable(tag: Tag) extends BaseTable[Group](tag, "groups") {
    def name = column[String]("name")

    def description = column[String]("description")

    def * = (id, name, description) <> (Group.tupled, Group.unapply)
  }

  val groupsTable = TableQuery[GroupsTable]

  class UsersGroupsTable(tag: Tag) extends Table[UserGroup](tag, "users_groups") {
    def userId = column[String]("userId")

    def groupId = column[Long]("groupId")

    def * = (userId, groupId) <> (UserGroup.tupled, UserGroup.unapply)
  }

  val usersGroupsTable = TableQuery[UsersGroupsTable]

  class AuthTokensTable(tag: Tag) extends Table[AuthToken](tag, "authTokens") {
    def id = column[String]("id")

    def userId = column[String]("userId")

    def expiry = column[String]("expiry")

    def * = (id, userId, expiry) <> ((AuthToken.apply _).tupled, AuthToken.unapply)
  }

  val authTokensTable = TableQuery[AuthTokensTable]

  class ArtifactsTable(tag: Tag) extends BaseTable[Artifact](tag, "artifacts") {
    def name = column[String]("name")

    def version = column[String]("version")

    def scope = column[String]("scope")

    def * = (id, name, version, scope) <> (Artifact.tupled, Artifact.unapply)
  }

  val artifactsTable = TableQuery[ArtifactsTable]

  class NodeTypesTable(tag: Tag) extends BaseTable[NodeType](tag, "nodetypes") {
    def name = column[String]("name")

    def description = column[String]("description")

    def tpe = column[String]("type")

    def className = column[String]("className")

    def properties = column[String]("properties")

    def widget = column[String]("widget")

    def endPoints = column[String]("endPoints")

    def artifact = column[String]("artifact")

    def * = (id, name, description, tpe, className, properties, widget, endPoints, artifact) <> (NodeType.tupled, NodeType.unapply)

  }

  val nodeTypesTable = TableQuery[NodeTypesTable]

  class PipelinesTable(tag: Tag) extends Table[Pipeline](tag, "pipelines") {
    def frontendId = column[String]("frontendId", O.PrimaryKey)

    def name = column[String]("name")

    def description = column[String]("description")

    def version = column[Long]("version")

    def status = column[String]("status")

    def jsonRepr = column[String]("jsonRepr")

    def createdBy = column[String]("createdBy")

    def updatedBy = column[String]("updatedBy")

    def accessedBy = column[String]("accessedBy")

    def * = (frontendId, name, description, version, status, jsonRepr, createdBy, updatedBy, accessedBy) <> (Pipeline.tupled, Pipeline.unapply)

  }

  val pipelinesTable = TableQuery[PipelinesTable]

  class SchemaFieldsTable(tag: Tag) extends Table[SchemaField](tag, "schemafields") {
    def schemaName1 = column[String]("schemaName") //incidentally, schemaName is already present in Table[]

    def schemaDescription = column[String]("schemaDescription")

    def fieldName = column[String]("fieldName")

    def fieldType = column[String]("fieldType")

    def createdBy = column[String]("createdBy")

    def updatedBy = column[String]("updatedBy")

    def accessedBy = column[String]("accessedBy")

    def pk = primaryKey("pkey", (schemaName1, fieldName))

    def * = (schemaName1, schemaDescription, fieldName, fieldType, createdBy, updatedBy, accessedBy) <> (SchemaField.tupled, SchemaField.unapply)

  }

  val schemaFieldsTable = TableQuery[SchemaFieldsTable]

}
