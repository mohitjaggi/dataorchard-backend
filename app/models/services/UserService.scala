/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.services

import java.util.UUID
import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile
import models.daos.UsersDAO
import models.entities.User
import play.api.libs.concurrent.Execution.Implicits._

import scala.concurrent.Future

class UserService @Inject()(userDAO: UsersDAO) extends IdentityService[User] {

  /**
    * Retrieves a user that matches the specified ID.
    *
    * @param id The ID to retrieve a user.
    * @return The retrieved user or None if no user could be retrieved for the given ID.
    */
  def retrieve(id: UUID) = userDAO.find(id)

  def list = userDAO.find
  /**
    * Retrieves a user that matches the specified login info.
    *
    * @param loginInfo The login info to retrieve a user.
    * @return The retrieved user or None if no user could be retrieved for the given login info.
    */
  def retrieve(loginInfo: LoginInfo): Future[Option[User]] = userDAO.find(loginInfo)

  /**
    * Saves a user.
    *
    * @param user The user to save.
    * @return The saved user.
    */
  def save(user: User) = userDAO.save(user)

  /**
    * Saves the social profile for a user.
    *
    * If a user exists for this profile then update the user, otherwise create a new user with the given profile.
    *
    * @param profile The social profile to save.
    * @return The user for whom the profile was saved.
    */
  def save(profile: CommonSocialProfile) = {
    userDAO.find(profile.loginInfo).flatMap {
      case Some(user) =>
        val newUser = user.copy(
          providerId = profile.loginInfo.providerID,
          providerKey = profile.loginInfo.providerKey,
          firstName = profile.firstName.getOrElse("John"),
          lastName = profile.lastName.getOrElse("Smith"),
          email = profile.email.getOrElse("john@smith.com"),
          avatarURL = profile.avatarURL)

        userDAO.save(newUser)

      case None => userDAO.save(User.fromExternal(UUID.randomUUID(),
        profile.loginInfo,
        profile.firstName,
        profile.lastName,
        profile.email,
        profile.avatarURL,
        true))
    }
  }
}
