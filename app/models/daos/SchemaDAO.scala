/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.daos

import models.entities._
import models.persistence.SlickTables
import models.persistence.SlickTables.SchemaFieldsTable
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SchemaDAO extends HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val tableQ: TableQuery[SchemaFieldsTable] = SlickTables.schemaFieldsTable

  def find = db.run(tableQ.result)

  def find(schemaName: String) = db.run(
    tableQ.filter(row => row.schemaName1 === schemaName)
      .result.map(f => if(f.nonEmpty) Some(Schema(f)) else None))

  def save(schema: Schema): Future[Schema] = {
    val schemaFields = schema.fields.map(f =>
      SchemaField(schema.schemaName, schema.schemaDescription,
        f.fieldName, f.fieldType))

    val toBeDeleted = tableQ.filter(row => row.schemaName1 === schema.schemaName).delete
    val toBeInserted = schemaFields.map { schemaField => tableQ.insertOrUpdate(schemaField) }
    val inOneGo = DBIO.sequence(toBeDeleted +: toBeInserted)
    db.run(inOneGo.transactionally).map(_ => schema)
  }

}
