/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.daos

import models.entities.{BaseEntity, JoinedEntity}
import models.persistence.SlickTables.BaseTable
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

trait TraitJoinedDAO[T1, T2, L, R, J]

abstract class JoinedDAO[T1 <: BaseTable[L], T2 <: BaseTable[R], L <: BaseEntity, R <: BaseEntity, J <: JoinedEntity]()
  extends TraitJoinedDAO[T1, T2, L, R, J] with HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  import dbConfig.driver.api._

  protected val tableLeftQ: TableQuery[T1]
  protected val tableRightQ: TableQuery[T2]


  def findByLeft(id : Long) = {
    val result = for {
      l <- tableLeftQ if l.id === id
      r <- tableRightQ
    } yield (l, r)
    db.run(result.result)
  }


}