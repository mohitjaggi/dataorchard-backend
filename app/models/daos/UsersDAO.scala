/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.daos

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import models.entities._
import models.persistence.SlickTables
import models.persistence.SlickTables.{GroupsTable, UsersGroupsTable, UsersTable}
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class UsersDAO extends HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val tableQ: TableQuery[UsersTable] = SlickTables.usersTable

  def find = db.run(tableQ.result)

  def find(loginInfo: LoginInfo) = db.run(
    tableQ.filter(row => row.providerId === loginInfo.providerID &&
      row.providerKey === loginInfo.providerKey)
      .result.headOption)

  def find(id: UUID) = db.run(
    tableQ.filter(_.userId === id.toString)
      .result.headOption)

  def save(user: User): Future[User] = {
    db.run(tableQ.insertOrUpdate(user)).map(_ => user)
  }

}

class GroupsDAO extends HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val tableQ: TableQuery[GroupsTable] = SlickTables.groupsTable

  def find = db.run(tableQ.result)

  def find(id: Long) = db.run(
    tableQ.filter(_.id === id)
      .result.headOption)

  def find(name: String) = db.run(
    tableQ.filter(_.name === name)
      .result.headOption)

  def save(group: Group): Future[Group] = {
    val insertQuery = tableQ returning tableQ.map(_.id) into ((g, id) => g.copy(id = id))
    val action = insertQuery += group
    db.run(action)
  }
}

class UsersGroupsDAO extends HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val tableQ: TableQuery[UsersGroupsTable] = SlickTables.usersGroupsTable

  def find = db.run(tableQ.result)

  def findByUser(userId: UUID) = db.run(
    tableQ.filter(_.userId === userId.toString)
      .result)

  def findByGroup(groupId: Long) = db.run(
    tableQ.filter(_.groupId === groupId)
      .result)

  def save(userGroup: UserGroup): Future[UserGroup] = {
    db.run(tableQ.insertOrUpdate(userGroup)).map(_ => userGroup)
  }
}

class UsersGroupsDetailDAO extends HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val userGroupsQ: TableQuery[UsersGroupsTable] = SlickTables.usersGroupsTable
  protected val usersQ: TableQuery[UsersTable] = SlickTables.usersTable
  protected val groupsQ: TableQuery[GroupsTable] = SlickTables.groupsTable

  def find = {
    val query = for {
      ((ug, u), g) <- userGroupsQ join usersQ on (_.userId === _.userId) join groupsQ on (_._1.groupId === _.id)
    } yield (ug, u, g)
    db.run(query.result).map { s =>
      s.map { case (_, u, g) =>
        UserGroupJoined(u, g)
      }
    }
  }
}
