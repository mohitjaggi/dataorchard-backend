/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.daos

import models.entities._
import models.persistence.SlickTables
import models.persistence.SlickTables.{ArtifactsTable, NodeTypesTable}
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

class NodeTypeWithArtifactDAO
  extends TraitJoinedDAO[NodeTypesTable, ArtifactsTable, NodeType, Artifact, NodeTypeWithArtifact]
    with HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val tableLeftQ: TableQuery[NodeTypesTable] = SlickTables.nodeTypesTable
  protected val tableRightQ: TableQuery[ArtifactsTable] = SlickTables.artifactsTable


  def findByTpe(tpe: String) = {
    val query = for {
      l <- tableLeftQ if l.tpe === tpe
      r <- tableRightQ if r.name === l.artifact
    } yield (l, r)
    db.run(query.result).map { s =>
      s.map { case (n, a) =>
        NodeTypeWithArtifact(n, a)
      }
    }
  }

  def findByNodeTypeName(name: String) = {
    val query = for {
      l <- tableLeftQ if l.name === name
      r <- tableRightQ if r.name === l.artifact
    } yield (l, r)
    db.run(query.result).map { s =>
      s.map { case (n, a) =>
        NodeTypeWithArtifact(n, a)
      }
    }
  }

}