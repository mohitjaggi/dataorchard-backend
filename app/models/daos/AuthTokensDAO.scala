/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.daos

import java.util.UUID

import models.entities.AuthToken
import models.persistence.SlickTables
import models.persistence.SlickTables.AuthTokensTable
import org.joda.time.DateTime
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class AuthTokensDAO extends HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val tableQ: TableQuery[AuthTokensTable] = SlickTables.authTokensTable

  def find(id: UUID) = db.run(
    tableQ.filter(_.id === id.toString)
      .result.headOption)

  def find = db.run(tableQ.result)

  def findExpired(dateTime: DateTime): Future[Seq[AuthToken]] = Future(List[AuthToken]())

  def save(token: AuthToken): Future[AuthToken] = db.run(tableQ.insertOrUpdate(token)).map(_ => token)

  def remove(id: UUID): Future[Unit] = Future { }

}