/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.daos

import models.entities._
import models.persistence.SlickTables.PipelinesTable
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile
import slick.lifted.CanBeQueryCondition

import scala.concurrent.Future

abstract class PipelineDAO extends HasDatabaseConfig[JdbcProfile] {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  import dbConfig.driver.api._

  protected val tableQ: TableQuery[PipelinesTable]

  def insert(row: Pipeline): Future[String] = {
    insert(Seq(row)).map(_.head)
  }

  def insert(rows: Seq[Pipeline]): Future[Seq[String]] = {
    db.run(tableQ returning tableQ.map(_.frontendId) ++= rows)
  }

  def insertOrUpdate(row: Pipeline) = {
    db.run(tableQ.insertOrUpdate(row))
  }

  def update(row: Pipeline): Future[Int] = {
      db.run(tableQ.filter(_.frontendId === row.frontendId).update(row))
  }

  def update(rows: Seq[Pipeline]): Future[Unit] = {
    db.run(DBIO.seq((rows.map(r => tableQ.filter(_.frontendId === r.frontendId).update(r))): _*))
  }

  def findById(id: String): Future[Option[Pipeline]] = {
    db.run(tableQ.filter(_.frontendId === id).result.headOption)
  }

  def findByName(name: String): Future[Option[Pipeline]] = {
    db.run(tableQ.filter(_.name === name).result.headOption)
  }

  def findPublishedByName(name: String): Future[Option[Pipeline]] = {
    db.run(tableQ.filter(p => p.name === name && p.status === "published").result.headOption)
  }

  def findPublishedByName(name: String, userId: String): Future[Option[Pipeline]] = {
    db.run(tableQ.filter(p => p.name === name && p.status === "published" && p.createdBy === userId).result.headOption)
  }

  def findByName(name: String, userId: String): Future[Option[Pipeline]] = {
    db.run(tableQ.filter(p => p.name === name && p.createdBy === userId).result.headOption)
  }


  def findAll: Future[Seq[Pipeline]] = {
    db.run(tableQ.result)
  }

  def findPublishedAll: Future[Seq[Pipeline]] = {
    db.run(tableQ.filter(_.status === "published").result)
  }

  def findDraftsAll: Future[Seq[Pipeline]] = {
    db.run(tableQ.filter(p => p.status === "draft").result)
  }

  def findAllForUser(userId: String): Future[Seq[Pipeline]] = {
    db.run(tableQ.filter(_.createdBy === userId).result)
  }

  def findPublishedForUser(userId: String): Future[Seq[Pipeline]] = {
    db.run(tableQ.filter(p => p.status === "published" && p.createdBy === userId).result)
  }

  def findDraftsForUser(userId: String): Future[Seq[Pipeline]] = {
    db.run(tableQ.filter(p => p.status === "draft" && p.createdBy === userId).result)
  }

  def findByFilter[C: CanBeQueryCondition](f: (PipelinesTable) => C): Future[Seq[Pipeline]] = {
    db.run(tableQ.withFilter(f).result)
  }

  def deleteById(id: String): Future[Int] = {
    deleteById(Seq(id))
  }

  def deleteById(ids: Seq[String]): Future[Int] = {
    db.run(tableQ.filter(_.frontendId.inSet(ids)).delete)
  }

  def deleteByFrontendId(id: String): Future[Int] = {
    deleteByFrontendId(Seq(id))
  }

  def deleteByFrontendId(ids: Seq[String]): Future[Int] = {
    db.run(tableQ.filter(_.frontendId.inSet(ids)).delete)
  }

  def deleteByFilter[C: CanBeQueryCondition](f: (PipelinesTable) => C): Future[Int] = {
    db.run(tableQ.withFilter(f).delete)
  }

}