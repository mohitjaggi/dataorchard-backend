/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.entities

case class Artifact(id: Long,
                    name: String,
                    version: String,
                    scope: String
                   ) extends BaseEntity

