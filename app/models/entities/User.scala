/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.entities

import java.util.UUID

import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.api.{Identity, LoginInfo}
import com.mohiva.play.silhouette.impl.providers.OAuth2Info
import play.api.libs.json.Json


case class User(
                 userIDStr: String,
                 providerId: String,
                 providerKey: String,
                 firstName: String,
                 lastName: String,
                 email: String,
                 avatarURL: Option[String],
                 activated: Boolean) extends Identity {

  def loginInfo = LoginInfo(providerId, providerKey)

  def userID = UUID.fromString(userIDStr)

  def name = firstName + " " + lastName
}


object User {
  val defaultUserId1 = "123e4567-e89b-12d3-a456-426655440000"
  val defaultUserId2 = "123e4567-e89b-12d3-a456-426655440001"

//  implicit val passwordInfoJsonFormat = Json.format[PasswordInfo]
//  implicit val oauth2InfoJsonFormat = Json.format[OAuth2Info]
//  //  implicit val profileJsonFormat = Json.format[Profile]
//  implicit val authUserJsonFormat = Json.format[User]

  def fromExternal(
                    uuid: UUID,
                    loginInfo: LoginInfo,
                    firstName: Option[String],
                    lastName: Option[String],
                    email: Option[String],
                    // passwordInfoJson: Option[PasswordInfo],
                    //oauth2InfoJson: Option[OAuth2Info],
                    avatarURL: Option[String],
                    confirmed: Boolean): User = new User(
    uuid.toString,
    loginInfo.providerID, loginInfo.providerKey,
    firstName.getOrElse(""),
    lastName.getOrElse(""),
    email.getOrElse(""),
   // Json.toJson(passwordInfoJson).toString(),
    //Json.toJson(oauth2InfoJson).toString(),
    avatarURL,
    confirmed)

  //  def unapply(user: AuthUser): Option[(UUID, LoginInfo, Boolean, String, String, String, Option[PasswordInfo], Option[OAuth2Info], Option[String])] = {
  //    Some((user.uuid, user.loginInfo, user.confirmed, user.email, user.firstName, user.lastName, user.passwordInfo, user.oauth2Info, user.avatarUrl))
  //  }

}


/*
  the following are used for handling ../configuration/user URIs
 */
case class NodeTypeVersion(name: String, version: String, scope: String)

case class NodeTypeDefaultVersion(nodeTypeName: String, nodeTypeVersion: NodeTypeVersion)

case class PipelineDrafts(isMigrated: Boolean,
                          default: Seq[Pipeline])

case class UserConfig(userId: Long,
                      nodeTypeDefaultVersion: NodeTypeDefaultVersion,
                      pipelineDrafts: PipelineDrafts) {

  def this(pds: Seq[Pipeline]) = {
    this(0, NodeTypeDefaultVersion("TextFileReader-batchsource-cdap-data-pipeline",
      NodeTypeVersion("cdap-data-pipeline", "3.5.1", "SYSTEM")), PipelineDrafts(true, pds))
  }
}

case class UserConfigWrapper(id: String, property: UserConfig)

case class Group(id: Long, name: String, description: String)

case class UserGroup(userIdStr: String, groupId: Long)

case class UserGroupJoined(user: User, group: Group)
