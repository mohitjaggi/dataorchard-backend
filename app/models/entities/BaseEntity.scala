/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.entities

trait BaseEntity {
  val id : Long
  def isValid = true
}