/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.entities

trait JoinedEntity {
  val idLeft : Long
  val idRight : Long
  def isValid = true
}