/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.entities

case class SchemaField(schemaName: String,
                       schemaDescription: String,
                       fieldName: String,
                       fieldType: String,
                       createdBy: String = "",
                       updatedBy: String = "",
                       accessedBy: String = "")

case class Schema(schemaName: String,
                  schemaDescription: String,
                  fields: Seq[SchemaField])

object Schema {
  def apply(fields: Seq[SchemaField]): Schema = {
    require(fields.length > 0, "No fields supplied")
    val n = fields(0).schemaName
    val d = fields(0).schemaDescription
    require(fields.forall(f => f.schemaName == n && f.schemaDescription == d))
    Schema(n, d, fields)
  }
}