/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.entities

case class NodeType(id: Long,
                    name: String,
                    description: String,
                    tpe: String,
                    className: String,
                    properties: String,
                    widget: String,
                    endPoints: String,
                    artifact: String
                   ) extends BaseEntity

case class NodeTypeWithArtifact(nodeType: NodeType,
                                artifact: Artifact)

