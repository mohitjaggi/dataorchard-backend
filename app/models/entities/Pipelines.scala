/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package models.entities

case class Pipeline(frontendId: String,
                    name: String,
                    description: String,
                    version: Long,
                    status: String,
                    jsonRepr: String,
                    createdBy: String,
                    updatedBy: String,
                    accessedBy: String
                   )

object PipelineStatus {

  sealed trait EnumVal

  case object Draft extends EnumVal

  case object Published extends EnumVal

  def fromString(status: String) = status match {
    case "draft" => Draft
    case "published" => Published
  }
}