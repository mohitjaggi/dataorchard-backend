package models.entities

import java.util.UUID

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

case class AuthToken(
                      idStr: String,
                      userIDStr: String,
                      expiryStr: String) {
  def id = UUID.fromString(idStr)

  def userID = UUID.fromString(userIDStr)

  def expiry = {
    val formatter = DateTimeFormat.forPattern(AuthToken.expiryFormat)
    formatter.parseDateTime(expiryStr)
  }

}

object AuthToken {

  val expiryFormat = "dd/MM/yyyy HH:mm:ss"
  //  implicit val passwordInfoJsonFormat = Json.format[PasswordInfo]
  //  implicit val oauth2InfoJsonFormat = Json.format[OAuth2Info]
  //  //  implicit val profileJsonFormat = Json.format[Profile]
  //  implicit val authUserJsonFormat = Json.format[User]

  def fromExternal(
                    id: UUID,
                    userId: UUID,
                    expiry: DateTime
                   ) = new AuthToken(id.toString, userId.toString, expiry.toString(expiryFormat))
}
