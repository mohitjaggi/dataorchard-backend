/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import java.time.Clock

import com.google.inject.{AbstractModule, Provides}
import models.daos._
import models.entities._
import models.persistence.SlickTables
import models.persistence.SlickTables._


/**
  * This class is a Guice module that tells Guice how to bind several
  * different types. This Guice module is created when the Play
  * application starts.
  *
  * Play will automatically use any class called `Module` that is in
  * the root package. You can create modules in other locations by
  * adding `play.modules.enabled` settings to the `application.conf`
  * configuration file.
  */
class Module extends AbstractModule {

  override def configure() = {
    // Use the system clock as the default implementation of Clock
    bind(classOf[Clock]).toInstance(Clock.systemDefaultZone)
  }

  @Provides
  def provideUsersDAO: UsersDAO = new UsersDAO {
    override protected val tableQ: dbConfig.driver.api.TableQuery[UsersTable] = SlickTables.usersTable
  }

  @Provides
  def provideGroupsDAO: GroupsDAO = new GroupsDAO {
    override protected val tableQ: dbConfig.driver.api.TableQuery[GroupsTable] = SlickTables.groupsTable
  }

  @Provides
  def provideUsersGroupsDAO: UsersGroupsDAO = new UsersGroupsDAO {
    override protected val tableQ: dbConfig.driver.api.TableQuery[UsersGroupsTable] = SlickTables.usersGroupsTable
  }

  @Provides
  def provideAuthTokensDAO: AuthTokensDAO = new AuthTokensDAO {
    override protected val tableQ: dbConfig.driver.api.TableQuery[AuthTokensTable] = SlickTables.authTokensTable
  }


  @Provides
  def provideNodeTypesDAO: BaseDAO[NodeTypesTable, NodeType] = new SimpleDAO[NodeTypesTable, NodeType] {
    override protected val tableQ: dbConfig.driver.api.TableQuery[NodeTypesTable] = SlickTables.nodeTypesTable
  }

  @Provides
  def provideArtifactsDAO: BaseDAO[ArtifactsTable, Artifact] = new SimpleDAO[ArtifactsTable, Artifact] {
    override protected val tableQ: dbConfig.driver.api.TableQuery[ArtifactsTable] = SlickTables.artifactsTable
  }


  @Provides
  def providePipelinesDAO: PipelineDAO = new PipelineDAO {
    override protected val tableQ: dbConfig.driver.api.TableQuery[PipelinesTable] = SlickTables.pipelinesTable
  }

  @Provides
  def provideNodeWithArtifactsDAO: NodeTypeWithArtifactDAO =
    new NodeTypeWithArtifactDAO {
      override protected val tableLeftQ: dbConfig.driver.api.TableQuery[NodeTypesTable] = SlickTables.nodeTypesTable
      override protected val tableRightQ: dbConfig.driver.api.TableQuery[ArtifactsTable] = SlickTables.artifactsTable
    }

}



