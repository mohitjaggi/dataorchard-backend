package utils.auth

import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.impl.authenticators.{BearerTokenAuthenticator, CookieAuthenticator}
import models.entities.User

/**
 * The cookie based auth env.
 */
trait CookieEnv extends Env {
  type I = User
  type A = CookieAuthenticator
}

/**
  * The bearer token based auth env.
  */
trait BearerTokenEnv extends Env {
  type I = User
  type A = BearerTokenAuthenticator
}
