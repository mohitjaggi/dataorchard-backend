/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package controllers

import javax.inject.Inject

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.services.AvatarService
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import controllers.JsonSerDes._
import forms.core.SchemaForm
import models.daos.SchemaDAO
import models.entities.{Schema, SchemaField}
import models.services.{AuthTokenService, UserService}
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.libs.mailer.MailerClient
import play.api.mvc.Controller
import utils.auth.CookieEnv

import scala.concurrent.Future

class SchemaController @Inject()(
                                  schemaDAO: SchemaDAO,
                                  val messagesApi: MessagesApi,
                                  silhouette: Silhouette[CookieEnv],
                                  userService: UserService,
                                  authInfoRepository: AuthInfoRepository,
                                  authTokenService: AuthTokenService,
                                  avatarService: AvatarService,
                                  passwordHasherRegistry: PasswordHasherRegistry,
                                  mailerClient: MailerClient,
                                  implicit val webJarAssets: WebJarAssets)
  extends Controller with I18nSupport {

  def view = silhouette.UnsecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.schema(SchemaForm.form)))
  }

  def tabularView = silhouette.UnsecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.schemaView()))
  }

  def tabularView2 = silhouette.UnsecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.tableView(
      title = "Schemas",
      restApiUrl = routes.SchemaController.list().url,
      columnFormat =
        """
          |[
          |        {"title":"Schema Name", "field":"name", "sorter":"string", "width":150},
          |        {"title":"Schema Description", "field":"description", "sorter":"string", "width":200}
          |]
        """.stripMargin
    )))
  }

  def submit = silhouette.UnsecuredAction.async { implicit request =>
    SchemaForm.form.bindFromRequest.fold(
      form => Future.successful(BadRequest(views.html.schema(form))),

      schemaForm => {
        val fields = schemaForm.fields.map(f => SchemaField(schemaForm.name, schemaForm.description, f.name, f.tpe))
        val schema = Schema(fields)
        schemaDAO.save(schema).map { _ =>
          Redirect(routes.ApplicationController.index()).flashing("success" -> Messages("schema.created"))
        }
      }
    )
  }

  def list = silhouette.UnsecuredAction.async { implicit request =>
    schemaDAO.find.map { schema =>
      val schemaList = schema.groupBy(_.schemaName).map { case (_, fields) => Schema(fields) }
      Ok(Json.toJson(schemaList))
    }
  }

  def find(schemaName: String) = silhouette.UnsecuredAction.async { implicit request =>
    schemaDAO.find(schemaName).map { schema => Ok(Json.toJson(schema)) }
  }

}
