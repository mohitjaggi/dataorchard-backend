/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag

import scalax.collection.Graph
import scalax.collection.GraphEdge.DiEdge


object Pipeline {
  type Dag = Graph[nodetypes.Node, DiEdge]
}

object Test extends App {

  import nodetypes._

  val input = TextFileReader("input", TextFileReader.Params(
    CommonParams.FileName("/tmp/x"),
    CommonParams.DeleteOnSuccess(false))).asInstanceOf[Node]

  val wordCounter = WordCounter("wc", WordCounter.Params(
    CommonParams.WordDelimiter(" "))).asInstanceOf[Node]

  val output = CsvFileWriter("output", CsvFileWriter.Params(
    CommonParams.FileName("/tmp/wcx"),
    CommonParams.FieldDelimiter(","),
    CommonParams.WriteHeader(true),
    CommonParams.SinglePart(true))).asInstanceOf[Node]

  val dagNodes = List(wordCounter, output, input)
  val dagEdges = List(DiEdge(input, wordCounter), DiEdge(wordCounter, output))
  val dag = Graph.from(dagNodes, dagEdges)

  val code = StringBuilder.newBuilder
  dag.topologicalSort.fold(
    cycleNode => println(s"cycle involving $cycleNode"),
    _.toLayered foreach {
      case (layer, nodes) =>
        println(s"Processing layer #$layer...")
        for (node <- nodes) {
          println(node.diPredecessors)
          println(node)
          code ++= node.sparkCode(node.diPredecessors.map(_.value))
        }
    }
  )
  println(code)
}
