/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package controllers

import javax.inject._

import models.daos.PipelineDAO
import models.entities.{Pipeline, User, UserConfig, UserConfigWrapper}
import play.api.Logger
import play.api.libs.json.{JsObject, JsString, Json}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import JsonSerDes._

@Singleton
class PipelinesController @Inject()(pipelinesDAO: PipelineDAO)
                                   (implicit exec: ExecutionContext)
  extends Controller {

  def getPipelines = Action.async { req =>
    Logger.debug(s"getPipelines $req")
    pipelinesDAO.findAll.map { pipes => Ok(Json.toJson(pipes)) }
  }

  def getPipeline(name: String) = Action.async { req =>
    Logger.debug(s"getPipeline $req")
    pipelinesDAO.findByName(name).map { pipes => Ok(Json.toJson(pipes)) }
  }


  def getPipelinesForUser = Action.async { req =>
    Logger.debug(s"getPipelinesForUser $req")
    pipelinesDAO.findAllForUser(User.defaultUserId1).map { pipes => Ok(Json.toJson(pipes)) }
  }

  def getPipelineForUser(name: String) = Action.async { req =>
    Logger.debug(s"getPipelineForUser $req")
    pipelinesDAO.findByName(name, User.defaultUserId1).map { pipes => Ok(Json.toJson(pipes)) }
  }

  def getUserConfig = Action.async { req =>
    Logger.debug(s"getUserConfig $req")
    import JsonSerDes.writesUserConfigWrapper
    pipelinesDAO.findDraftsAll.map { pipes =>
      Ok(
        Json.toJson(UserConfigWrapper("", new UserConfig(pipes))))
    }
  }

  def getUserConfigForUser = Action.async { req =>
    Logger.debug(s"getUserConfig $req")

    pipelinesDAO.findDraftsForUser(User.defaultUserId1).map { pipes =>
      Ok(
        Json.toJson(UserConfigWrapper("", new UserConfig(pipes)))
                    (JsonSerDes.writesUserConfigWrapper)
      )
    }
  }

  private def transformJsonRepr(jsonStr: String) = {
    // stringify the config field as expected by UI
    val pipeJson = Json.parse(jsonStr)
    val configJson = (pipeJson \ "config").get
    Logger.debug(configJson.toString())
    Logger.debug("----")
    Logger.debug(Json.asciiStringify(configJson))
    Logger.debug("-----")
    val configAsString = Json.asciiStringify(configJson)
    //    val configAsString = Json.asciiStringify(JsString(configJson.toString))
    Logger.debug(configAsString)
    Logger.debug("----")
    val configJsonObj = Json.obj("config" -> JsString(configAsString))
    Logger.debug(configJsonObj.toString())
    pipeJson.as[JsObject] ++ configJsonObj //++ Json.obj("version" -> "3.5.1")
  }

  def getPublishedPipelinesForUser(scope: Option[String]) = Action.async { req =>
    Logger.debug(s"getPublishedPipelinesForUser $scope $req")
    pipelinesDAO.findPublishedForUser(User.defaultUserId1).map { pipes =>
      val pipesJson = pipes.map { pipe => transformJsonRepr(pipe.jsonRepr) }
      Ok(Json.toJson(pipesJson))
    }
  }

  def getPublishedPipelines = Action.async { req =>
    Logger.debug(s"getPipelines $req")
    pipelinesDAO.findPublishedAll.map { pipes =>
      val pipesJson = pipes.map { pipe => transformJsonRepr(pipe.jsonRepr) }
      Ok(Json.toJson(pipesJson))
    }
  }

  def getPublishedPipelineForUser(name: String) = Action.async { req =>
    Logger.debug(s"getPublishedPipelineForUser $name $req")
    pipelinesDAO.findPublishedByName(name, User.defaultUserId1).map { pipe =>
      pipe.fold(NoContent) {
        p =>
          val out = transformJsonRepr(p.jsonRepr)
          Ok(Json.toJson(out))
      }
    }
  }

  def getPublishedPipeline(name: String) = Action.async { req =>
    Logger.debug(s"getPublishedPipeline $name $req")
    pipelinesDAO.findPublishedByName(name).map { pipe =>
      pipe.fold(NoContent) {
        p =>
          val out = transformJsonRepr(p.jsonRepr)
          Ok(Json.toJson(out))
      }
    }
  }

  def putUserConfig() = Action.async(parse.json) {
    request => {
      Logger.debug(s"putUserConfig $request")
      Logger.debug(request.body.toString())
      val drafts = (request.body \ "hydratorDrafts" \ "default").as[JsObject]
      var i = 0
      for ((id, draft) <- drafts.fields) {
        val name = (draft \ "name").as[String]
        val desc = (draft \ "description").as[String]
        val version = System.currentTimeMillis()
        val pd = Pipeline(id, name, desc, version, "draft", Json.prettyPrint(draft),
          User.defaultUserId1, User.defaultUserId1, User.defaultUserId1)
        pipelinesDAO.insertOrUpdate(pd)
        i += 1
      }
      Future(Ok(s"$i"))
    }
  }

  def publishPipeline(name: String) = Action.async(parse.json) {
    request => {
      Logger.debug(s"publishPipeline $request")
      Logger.debug(request.body.toString())

      val draftId = (request.body \ "__ui__" \ "draftId").asOpt[String]
      val id = draftId.getOrElse(scala.util.Random.nextString(20))
      val name = (request.body \ "name").as[String]
      val description = (request.body \ "description").as[String]
      val version = System.currentTimeMillis()

      val pd = Pipeline(id, name, description, version, "published", Json.prettyPrint(request.body),
        User.defaultUserId1, User.defaultUserId1, User.defaultUserId1)
      pipelinesDAO.insertOrUpdate(pd)

      Future(Ok)
    }
  }
}