/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package controllers

import javax.inject._

import models.daos.{BaseDAO, NodeTypeWithArtifactDAO}
import models.entities.NodeType
import models.persistence.SlickTables.NodeTypesTable
import play.api.libs.json.Json
import play.api.mvc._
import play.api.Logger

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NodeTypesController @Inject()(nodeTypesDAO: BaseDAO[NodeTypesTable, NodeType],
                                    nodeTypeWithArtifactDAO: NodeTypeWithArtifactDAO)
                                   (implicit exec: ExecutionContext)
  extends Controller {

  def getNodeType(id: Long) = Action.async {
    Logger.debug(s"getNodeType $id")
    import controllers.JsonSerDes.writesNodeType
    nodeTypesDAO.findById(id) map { nodeType => nodeType.fold(NoContent)(nodeType => Ok(Json.toJson(nodeType))) }
  }

  def getNodeTypeByName(artName: String,
                        artVersion: String,
                        extName: String,
                        nodeTypeName: String,
                        scope: Option[String]) = Action.async { req =>
    Logger.debug(s"getNodeTypeByName $req")
    import controllers.JsonSerDes.writesNodeTypeDetails
    nodeTypeWithArtifactDAO.findByNodeTypeName(nodeTypeName) map { nodeType => Ok(Json.toJson(nodeType)) }
  }

  def getNodeTypes = Action.async {
    Logger.debug("getNodeTypes")
    import controllers.JsonSerDes.writesNodeType
    findNodeTypesAll map { nodeType => Ok(Json.toJson(nodeType)) }
  }

  private def findNodeTypesAll: Future[Seq[NodeType]] = {
    nodeTypesDAO.findByFilter(_ => true)
  }

  def getNodeTypesByType(name: String, version: String, tpe: String) = Action.async { request =>
    Logger.debug(s"getNodeTypesByType $request")
    import slick.driver.H2Driver.api._ //ideally we should not need the driver here but the filter needs it :(
    import controllers.JsonSerDes.writesNodeType
    nodeTypesDAO.findByFilter(x => x.tpe === tpe) map { nodeType => Ok(Json.toJson(nodeType)) }
  }

  def getNodeTypesByType2(artifact: String, version: String, tpe: String, scope: Option[String]) = Action.async { request =>
    Logger.debug(s"$request in getNodeTypesByType2")
    import slick.driver.H2Driver.api._ //ideally we should not need the driver here but the filter needs it :(
    import controllers.JsonSerDes.writesNodeType
    nodeTypesDAO.findByFilter(n => n.tpe === tpe) map { nodeType => Ok(Json.toJson(nodeType)) }
  }

  def getNodeTypesByType3(artifact: String, version: String, tpe: String, scope: Option[String]) = Action.async { request =>
    Logger.debug(s"$request in getNodeTypesByType3")
    import controllers.JsonSerDes.writesNodeTypeWithArtifact
    nodeTypeWithArtifactDAO.findByTpe(tpe) map { nodeType => Ok(Json.toJson(nodeType)) }
  }

  def getNodeTypeProperties(artifact: String, version: String, keys: String, scope: Option[String]) = Action.async { req =>
    Logger.debug(s"$req in getNodeTypeProperties")
    val split = keys.split(Array('.', '-')) // example: widgets.TextFileReader-batchsource
    require(split(0) == "widgets", s"Unexpected REST API query param: $keys in $req")
    val name = split(1)
    val tpe = split(2)
    import slick.driver.H2Driver.api._

    def makeResponse(json: String) = {
      Json.obj(keys -> json)
    }

    nodeTypesDAO.findByFilter(n => n.name === name && n.tpe === tpe)
        .map { ns => ns.headOption }
        .map { n => n.fold(NoContent)(n => Ok(makeResponse(n.widget))) }
  }

  def getSchema(artifact: String, version: String, pluginType: String, nodeTypeName: String, method: String) = Action { req =>
    import macros.NodeTypeMacros._
    Logger.debug(s"$req in getSchema")
    Logger.debug(Json.prettyPrint(req.body.asJson.get))
    getOutputSchema(s"dag.nodetypes.$nodeTypeName", req.body.asJson.get)

    BadRequest("not working yet")
  }

  def insertNodeType() = Action.async(parse.json) {
    request => {
      {
        for {
          name <- (request.body \ "name").asOpt[String]
          tpe <- (request.body \ "type").asOpt[String]
          className <- (request.body \ "className").asOpt[String]
          artifact <- (request.body \ "artifact").asOpt[String]
          properties <- (request.body \ "properties").asOpt[String]
          widget <- (request.body \ "widget").asOpt[String]
          endPoints <- (request.body \ "endPoints").asOpt[String]
          description <- (request.body \ "description").asOpt[String]
        } yield {
          nodeTypesDAO.insert(NodeType(0, name, description, tpe, className, properties, widget, endPoints, artifact)) map { n =>
            Ok(s"Id of User Added : $n")
          }
        }
      }.getOrElse(Future {
        BadRequest("Wrong json format")
      })
    }
  }

}
