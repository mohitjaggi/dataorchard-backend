/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package controllers

import models.entities._
import play.api.libs.json.{Json, Writes}

object JsonSerDes {

  /*
      automated mappings
   */
  implicit val userFormat = Json.format[User]

  implicit val groupFormat = Json.format[Group]

  implicit val userGroupFormat = Json.format[UserGroup]

  implicit val pipelineFormat = Json.format[Pipeline]

  implicit val schemaFieldFormat = Json.format[SchemaField]

  implicit val schemaFormat = Json.format[Schema]

  implicit val userGroupJoined = Json.format[UserGroupJoined]

  /*
      custom mappings
   */
  implicit val authTokenWrites = new Writes[AuthToken] {
    def writes(authToken: AuthToken) = Json.obj(
      "id" -> authToken.idStr,
      "userId" -> authToken.userIDStr,
      "expiry" -> authToken.expiryStr
    )
  }

  implicit val writesNodeType = new Writes[NodeType] {
    def writes(nodeType: NodeType) = Json.obj(
      "id" -> nodeType.id,
      "name" -> nodeType.name,
      "description" -> nodeType.description,
      "properties" -> nodeType.properties,
      "artifact" -> nodeType.artifact,
      "className" -> nodeType.className
    )
  }

  implicit val writesArtifact = new Writes[Artifact] {
    def writes(artifact: Artifact) = Json.obj(
      "id" -> artifact.id,
      "name" -> artifact.name,
      "version" -> artifact.version,
      "scope" -> artifact.scope
    )
  }

  implicit val writesNodeTypeWithArtifact = new Writes[NodeTypeWithArtifact] {
    def writes(n: NodeTypeWithArtifact) = Json.obj(
      "name" -> n.nodeType.name,
      "type" -> n.nodeType.tpe,
      "className" -> n.nodeType.className,
      "artifact" -> Json.toJson(n.artifact)(writesArtifact)
    )
  }

  implicit val writesNodeTypeDetails = new Writes[NodeTypeWithArtifact] {
    def writes(n: NodeTypeWithArtifact) = Json.obj(
      "name" -> n.nodeType.name,
      "type" -> n.nodeType.tpe,
      "className" -> n.nodeType.className,
      "properties" -> Json.parse(n.nodeType.properties),
      "endPoints" -> Json.parse(n.nodeType.endPoints),
      "artifact" -> Json.toJson(n.artifact)(writesArtifact)
    )
  }



  val writesPipelineSeq = new Writes[Seq[Pipeline]] {
    def writes(ps: Seq[Pipeline]) = {
      val kvs = ps.map { p => (p.frontendId, Json.toJsFieldJsValueWrapper(Json.parse(p.jsonRepr))) }
      Json.obj(kvs: _*)
    }
  }

  val writesPipelineDraft = new Writes[PipelineDrafts] {
    def writes(pd: PipelineDrafts) = Json.obj(
      "isMigrated" -> pd.isMigrated,
      "default" -> Json.toJson(pd.default)(writesPipelineSeq)
    )
  }

  val writesNodeTypeVersion = new Writes[NodeTypeVersion] {
    def writes(n: NodeTypeVersion) = Json.obj(
      "name" -> n.name,
      "version" -> n.version,
      "scope" -> n.scope
    )
  }

  val writesNodeTypeDefaultVersion = new Writes[NodeTypeDefaultVersion] {
    def writes(n: NodeTypeDefaultVersion) = Json.obj(
      n.nodeTypeName -> Json.toJson(n.nodeTypeVersion)(writesNodeTypeVersion)
    )
  }

  implicit val writesUserConfig = new Writes[UserConfig] {
    def writes(uc: UserConfig) = Json.obj(
      "plugin-default-version" -> Json.toJson(uc.nodeTypeDefaultVersion)(writesNodeTypeDefaultVersion),
      "hydratorDrafts" -> Json.toJson(uc.pipelineDrafts)(writesPipelineDraft)
    )
  }

  implicit val writesUserConfigWrapper = new Writes[UserConfigWrapper] {
    def writes(ucw: UserConfigWrapper) = Json.obj(
      "id" -> ucw.id,
      "property" -> Json.toJson(ucw.property)(writesUserConfig)
    )
  }

}
