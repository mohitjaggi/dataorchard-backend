package controllers

import javax.inject.Inject

import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.{LoginEvent, LoginInfo, LogoutEvent, Silhouette}
import com.mohiva.play.silhouette.impl.exceptions.IdentityNotFoundException
import com.mohiva.play.silhouette.impl.providers.{BasicAuthProvider, SocialProviderRegistry}
import models.services.UserService
import play.api.Configuration
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.Controller
import utils.auth.{BearerTokenEnv, CookieEnv}

import scala.concurrent.Future

/**
  * The basic application controller.
  *
  * @param messagesApi            The Play messages API.
  * @param silhouette             The Silhouette stack.
  * @param socialProviderRegistry The social provider registry.
  * @param webJarAssets           The webjar assets implementation.
  */
class ApplicationController @Inject()(
                                       val messagesApi: MessagesApi,
                                       silhouette: Silhouette[CookieEnv],
                                       socialProviderRegistry: SocialProviderRegistry,
                                       implicit val webJarAssets: WebJarAssets)
  extends Controller with I18nSupport {

  /**
    * Handles the index action.
    *
    * @return The result to display.
    */
  def index = silhouette.SecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.home(request.identity)))
  }

  /**
    * Handles the Sign Out action.
    *
    * @return The result to display.
    */
  def signOut = silhouette.SecuredAction.async { implicit request =>
    val result = Redirect(routes.ApplicationController.index())
    silhouette.env.eventBus.publish(LogoutEvent(request.identity, request))
    silhouette.env.authenticatorService.discard(request.authenticator, result)
  }
}

class BearerApplicationController @Inject()(
                                             val messagesApi: MessagesApi,
                                             silhouette: Silhouette[BearerTokenEnv],
                                             socialProviderRegistry: SocialProviderRegistry,
                                             basicAuthProvider: BasicAuthProvider,
                                             userService: UserService,
                                             configuration: Configuration,
                                             implicit val webJarAssets: WebJarAssets)
  extends Controller with I18nSupport {
  import scala.concurrent.ExecutionContext.Implicits.global
  /**
    * Handles the index action.
    *
    * @return The result to display.
    */
  def index = silhouette.SecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.home(request.identity)))
  }

  def getToken = silhouette.UnsecuredAction.async { implicit request =>
    println(request)
    println(request.headers)
    basicAuthProvider.authenticate(request).flatMap { mayBeLoginInfo: Option[LoginInfo] =>
      mayBeLoginInfo match {
        case Some(loginInfo) =>
          userService.retrieve(loginInfo).flatMap {
            case Some(user) if !user.activated =>
              Future.successful(Ok(views.html.activateAccount(user.email)))
            case Some(user) =>
              silhouette.env.authenticatorService.create(loginInfo).flatMap { authenticator =>
                silhouette.env.eventBus.publish(LoginEvent(user, request))
                silhouette.env.authenticatorService.init(authenticator).flatMap { token =>
                  val response = Json.obj(
                    "access_token" -> token,
                    "token_type" -> "Bearer",
                    "expires_in" -> 86400
                  )
                  Future.successful(Ok(response))
                }
              }
            case None => Future.failed(new IdentityNotFoundException("Couldn't find user"))
          }
        case None => Future.failed(new IdentityNotFoundException("Couldn't find user"))
      }
    }.recover {
      case e: ProviderException =>
        Redirect(routes.SignInController.view()).flashing("error" -> Messages("invalid.credentials"))
    }
  }

  /**
    * Handles the Sign Out action.
    *
    * @return The result to display.
    */
  def signOut = silhouette.SecuredAction.async {
    implicit request =>
      val result = Redirect(routes.BearerApplicationController.index())
      silhouette.env.eventBus.publish(LogoutEvent(request.identity, request))
      silhouette.env.authenticatorService.discard(request.authenticator, result)
  }
}
