/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package controllers

import javax.inject._

import controllers.JsonSerDes._
import models.daos.{GroupsDAO, UsersDAO, UsersGroupsDAO, UsersGroupsDetailDAO}
import models.entities.{Group, UserGroup}
import play.api.Logger
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.{Controller, _}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UsersController @Inject()(usersDAO: UsersDAO,
                                groupsDAO: GroupsDAO,
                                userGroupDAO: UsersGroupsDAO,
                                userGroupsDetailDAO: UsersGroupsDetailDAO,
                                implicit val webJarAssets: WebJarAssets,
                                val messagesApi: MessagesApi)
                               (implicit exec: ExecutionContext) extends Controller with I18nSupport  {

  def listUsers = Action.async {
    usersDAO.find.map { u => Ok(Json.toJson(u)) }
  }

  // putUser --> this is done by SignUpController

  def listGroups = Action.async {
    groupsDAO.find.map { g => Ok(Json.toJson(g)) }
  }

  def putGroup() = Action.async(parse.json) {
    request => {
      val group = request.body.as[Group]
      groupsDAO.save(group) map { g => Ok(Json.toJson(g)) }
    }
  }

  def listUserGroups = Action.async {
    userGroupDAO.find.map { ug => Ok(Json.toJson(ug)) }
  }

  def listUserGroupsDetail = Action.async {
    userGroupsDetailDAO.find.map { ug => Ok(Json.toJson(ug)) }
  }

  object Helper {
    case class UGFlat (userId: String, userEmail: String, groupName: String)
    implicit val ugFlatFormat = Json.format[UGFlat]
  }

  def listUserGroupsDetailFlattened = Action.async {
    import Helper._
    userGroupsDetailDAO.find.map { ugs =>
      val flat = ugs.map { ug => UGFlat(ug.user.userIDStr, ug.user.email, ug.group.name) }
      Ok(Json.toJson(flat))
    }
  }

  def putUserGroup() = Action.async(parse.json) {
    request => {
      val userGroup = request.body.as[UserGroup]
      userGroupDAO.save(userGroup) map { ug => Ok(Json.toJson(ug)) }
    }
  }

  def viewUsers = Action.async { implicit request =>
    Future.successful(Ok(views.html.tableView(
      title = "Users",
      restApiUrl = routes.ActivateAccountController.listUsers.url,
      columnFormat =
        """
          |[
          |        {"title":"First Name", "field":"firstName", "sorter":"string"},
          |        {"title":"Last Name", "field":"lastName", "sorter":"string"},
          |        {"title":"Email", "field":"email", "sorter":"string"},
          |        {"title":"Activated", "field":"activated", "sorter":"string"},
          |        {"title":"UUID", "field":"userIDStr", "sorter":"string"},
          |        {"title":"Provider", "field":"providerId", "sorter":"string"},
          |        {"title":"Provider Id", "field":"providerKey", "sorter":"string"}
          |]
        """.stripMargin
    )))
  }

  def viewGroups = Action.async { implicit request =>
    Future.successful(Ok(views.html.tableView(
      title = "Groups",
      restApiUrl = routes.UsersController.listGroups.url,
      columnFormat =
        """
          |[
          |        {"title":"Group ID", "field":"id", "sorter":"string"},
          |        {"title":"Name", "field":"name", "sorter":"string"},
          |        {"title":"Description", "field":"description", "sorter":"string"}
          |]
        """.stripMargin
    )))
  }

  def viewUserGroupsDetail = Action.async { implicit request =>
    Future.successful(Ok(views.html.tableView(
      title = "Groups",
      restApiUrl = routes.UsersController.listUserGroupsDetailFlattened.url,
      columnFormat =
        """
          |[
          |        {"title":"User ID", "field":"userId", "sorter":"string"},
          |        {"title":"User Email", "field":"userEmail", "sorter":"string"},
          |        {"title":"Group ID", "field":"groupName", "sorter":"string"}
          |]
        """.stripMargin
    )))
  }
}
