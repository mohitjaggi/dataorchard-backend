/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package controllers

import play.api.Logger
import play.api.mvc._

class CDAPController extends Controller {

  def redirect(path: String) = Action { req =>
    Logger.warn(s"-- redirecting ${req.method} to http://localhost:10000/$path?${req.rawQueryString}")
    Redirect(s"http://localhost:10000/$path", req.queryString, TEMPORARY_REDIRECT )
  }

  def ping = Action {
    val response =
      """
        |OK.
      """.stripMargin
    Ok(response).as(TEXT)
  }

  def getNamespaces = Action {
    val response =
      """
        |[{"name":"default","description":"Default Namespace","config":{"scheduler.queue.name":""}}]
      """.stripMargin
    Ok(response).as(JSON)
  }

  def getServicesStatus = Action {
    val response =
      """
        |{"streams":"OK","metrics.processor":"OK","appfabric":"OK","explore.service":"OK",
        |"dataset.executor":"OK","metadata.service":"OK","metrics":"OK","transaction":"OK","log.saver":"OK"}
      """.stripMargin
    Ok(response).as(JSON)
  }

  def getVersion = Action {
    val response =
      """
        |{"version":"3.5.1"}
      """.stripMargin
    Ok(response).as(JSON)
  }

  def getStreams = Action {
    Ok("[]").as(JSON)
  }

  def getDataSets = Action {
    Ok("[]").as(JSON)
  }

  def getRuns(name: String) = Action {
    Ok("[]").as(JSON)
  }

  def getMetrics = Action {
    val response =
      """
        |{"qid":
        |   {
        |   "startTime":1480702962,"endTime":1480706562,
        |   "series":
        |      [
        |      ]
        |   }
        |}
      """.stripMargin
    Ok(response).as(JSON)
  }


}
