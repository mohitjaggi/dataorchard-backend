package controllers

import java.util.UUID
import javax.inject.Inject

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.services.AvatarService
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import com.mohiva.play.silhouette.impl.providers._
import forms.auth.SignUpForm.Data
import forms.auth.SignUpForm
import models.entities.User
import models.services.{AuthTokenService, UserService}
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.mailer.{Email, MailerClient}
import play.api.mvc.Controller
import utils.auth.CookieEnv

import scala.concurrent.Future

/**
  * The `Sign Up` controller.
  *
  * @param messagesApi            The Play messages API.
  * @param silhouette             The Silhouette stack.
  * @param userService            The user service implementation.
  * @param authInfoRepository     The auth info repository implementation.
  * @param authTokenService       The auth token service implementation.
  * @param avatarService          The avatar service implementation.
  * @param passwordHasherRegistry The password hasher registry.
  * @param mailerClient           The mailer client.
  * @param webJarAssets           The webjar assets implementation.
  */
class SignUpController @Inject()(
                                  val messagesApi: MessagesApi,
                                  silhouette: Silhouette[CookieEnv],
                                  userService: UserService,
                                  authInfoRepository: AuthInfoRepository,
                                  authTokenService: AuthTokenService,
                                  avatarService: AvatarService,
                                  passwordHasherRegistry: PasswordHasherRegistry,
                                  mailerClient: MailerClient,
                                  implicit val webJarAssets: WebJarAssets)
  extends Controller with I18nSupport {

  /**
    * Views the `Sign Up` page.
    *
    * @return The result to display.
    */
  def view = silhouette.UnsecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.signUp(SignUpForm.form)))
  }

  /**
    * Handles the submitted form.
    *
    * @return The result to display.
    */
  def submit = silhouette.UnsecuredAction.async { implicit request =>
    SignUpForm.form.bindFromRequest.fold(
      form => Future.successful(BadRequest(views.html.signUp(form))),
      data => {
        val result = Redirect(routes.SignUpController.view()).flashing("info" -> Messages("sign.up.email.sent", data.email))
        val loginInfo = LoginInfo(CredentialsProvider.ID, data.email)
        userService.retrieve(loginInfo).flatMap {
          case Some(user) =>
            val url = routes.SignInController.view().absoluteURL()
            mailerClient.send(Email(
              subject = Messages("email.already.signed.up.subject"),
              from = Messages("email.from"),
              to = Seq(data.email),
              bodyText = Some(views.txt.emails.alreadySignedUp(user, url).body),
              bodyHtml = Some(views.html.emails.alreadySignedUp(user, url).body)
            ))

            Future.successful(result)
          case None =>
            val authInfo = passwordHasherRegistry.current.hash(data.password)
            val user = User.fromExternal(
              uuid = UUID.randomUUID(),
              loginInfo = loginInfo,
              firstName = Some(data.firstName),
              lastName = Some(data.lastName),
              email = Some(data.email),
              avatarURL = None,
              confirmed = false
            )
            for {
              avatar <- avatarService.retrieveURL(data.email)
              user <- userService.save(user.copy(avatarURL = avatar))
              authInfo <- authInfoRepository.add(loginInfo, authInfo)
              authToken <- authTokenService.create(user.userID)
            } yield {
              val url = routes.ActivateAccountController.activate(authToken.id).absoluteURL()
              mailerClient.send(Email(
                subject = Messages("email.sign.up.subject"),
                from = Messages("email.from"),
                to = Seq(data.email),
                bodyText = Some(views.txt.emails.signUp(user, url).body),
                bodyHtml = Some(views.html.emails.signUp(user, url).body)
              ))

              silhouette.env.eventBus.publish(SignUpEvent(user, request))
              result
            }
        }
      }
    )
  }

  def defaultUser = silhouette.UnsecuredAction.async { implicit request =>
    val data = Data("test", "user", "test@gmail.com", "datagears")
    val loginInfo = LoginInfo(CredentialsProvider.ID, data.email)
    val authInfo = passwordHasherRegistry.current.hash(data.password)
    val user = User.fromExternal(
      uuid = UUID.fromString(User.defaultUserId1),
      loginInfo = loginInfo,
      firstName = Some(data.firstName),
      lastName = Some(data.lastName),
      email = Some(data.email),
      avatarURL = None,
      confirmed = true
    )
    for {
      avatar <- avatarService.retrieveURL(data.email)
      user <- userService.save(user.copy(avatarURL = avatar))
      _ <- authInfoRepository.add(loginInfo, authInfo)
      _ <- authTokenService.create(user.userID)
    } yield {
      silhouette.env.eventBus.publish(SignUpEvent(user, request))
      Ok("okay!")
    }
  }

  def defaultUser2 = silhouette.UnsecuredAction.async { implicit request =>
    val data = Data("test", "user", "test@gmail.com", "datagears")
    val loginInfo = LoginInfo(BasicAuthProvider.ID, data.email)
    val authInfo = passwordHasherRegistry.current.hash(data.password)
    val user = User.fromExternal(
      uuid = UUID.fromString(User.defaultUserId2),
      loginInfo = loginInfo,
      firstName = Some(data.firstName),
      lastName = Some(data.lastName),
      email = Some(data.email),
      avatarURL = None,
      confirmed = true
    )
    for {
      avatar <- avatarService.retrieveURL(data.email)
      user <- userService.save(user.copy(avatarURL = avatar))
      _ <- authInfoRepository.add(loginInfo, authInfo)
      _ <- authTokenService.create(user.userID)
    } yield {
      silhouette.env.eventBus.publish(SignUpEvent(user, request))
      Ok("okay2!")
    }
  }

}
