/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package controllers

import javax.inject._

import controllers.JsonSerDes._
import models.daos.BaseDAO
import models.entities.Artifact
import models.persistence.SlickTables.ArtifactsTable
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ArtifactsController @Inject()(artifactsDAO: BaseDAO[ArtifactsTable, Artifact])
                                   (implicit exec: ExecutionContext)
  extends Controller {

  def findArtifactsAll: Future[Seq[Artifact]] = {
    artifactsDAO.findByFilter(_ => true)
  }

  def getArtifacts(scope: Option[String]) = Action.async { req =>
    Logger.debug(s"getArtifacts $scope $req")
    findArtifactsAll map { art => Ok(Json.toJson(art)) }
  }

  def getArtifactExtensions(name: String, version: String, scope: Option[String]) = Action { req =>
    Logger.debug(s"$req in getArtifactExtensions $name $version")
    Ok(
      """
        |[
        | "batchsource", "batchsink", "batchaggregator", "transform"
        |]
      """.stripMargin).as(JSON)
  }

}