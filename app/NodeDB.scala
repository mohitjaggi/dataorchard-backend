/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import dag.nodetypes._
import models.daos.{BaseDAO, SimpleDAO}
import models.entities.NodeType
import models.persistence.SlickTables
import models.persistence.SlickTables.NodeTypesTable
import play.api.libs.json.{JsObject, JsString, Json}

object NodeDB {

  def nameToNodeType(name: String): NodeType = {
    import scala.reflect.runtime.{universe => ru}

    val runtimeMirror = ru.runtimeMirror(getClass.getClassLoader)
    val module = runtimeMirror.staticModule(name)
    val obj = runtimeMirror.reflectModule(module)
    val nodeMeta = obj.instance.asInstanceOf[NodeMeta]

    NodeType(id = 0,
      name = nodeMeta.name,
      description = nodeMeta.description,
      tpe = nodeMeta.tpe,
      className = "dummy",
      properties = nodeParamToJson(name),
      widget = nodeParamToWidgetJson(name).toString(),
      endPoints = "[]",
      artifact = "cdap-data-pipeline")
  }

  def nodeParamToJson(name: String): String = {
    import scala.reflect.runtime.{universe => ru}

    val fieldName = Map(
      "name" -> "name",
      "description" -> "description",
      "typeString" -> "type",
      "required" -> "required",
      "defaultJsonString" -> "default")

    val runtimeMirror = ru.runtimeMirror(getClass.getClassLoader)
    val classSym = runtimeMirror.classSymbol(Class.forName(s"$name$$Params"))
    val classMirror = runtimeMirror.reflectClass(classSym)
    val paramTpes = classSym.selfType.decls.collect {
      case method: ru.MethodSymbol if method.isCaseAccessor => method.typeSignature
    }

    val x = for (paramTpe <- paramTpes) yield {
      val configTpe = paramTpe.member(ru.TermName("config")).asTerm.typeSignature
      val cm = runtimeMirror.reflectClass(paramTpe.typeSymbol.asClass)
      val ctor = paramTpe.decl(ru.termNames.CONSTRUCTOR).asMethod
      val ctorm = cm.reflectConstructor(ctor)
      val ctorParamTpe = ctor.typeSignature.paramLists.head.head.typeSignature
      val paramFileName = if (ctorParamTpe =:= ru.typeOf[String])
        ctorm("x") //if string, boolean etc
      else if (ctorParamTpe =:= ru.typeOf[Boolean])
        ctorm(false)
      else if (ctorParamTpe =:= ru.typeOf[Integer])
        ctorm(0)
      else if (ctorParamTpe =:= ru.typeOf[Seq[String]])
        ctorm(List("x"))
      else
        throw new Exception(s"Not supported $ctorParamTpe")

      val configTermSym = paramTpe.member(ru.TermName("config"))
      val config = runtimeMirror.reflect(paramFileName).reflectField(configTermSym.asTerm).get

      var paramName = "dummy"
      var paramRequired = true
      val v = (for (field <- List("name", "description", "typeString", "required", "defaultJsonString")) yield {
        val sym = runtimeMirror.reflect(config).symbol.asType.toType.member(ru.TermName(field))
        val value = runtimeMirror.reflect(config).reflectField(sym.asTerm).get
        paramName = if (field == "name") value.toString else paramName
        paramRequired = field == "required" && value == true
        val valueFormatted = if (value == true || value == false || field == "defaultJsonString")
          value.toString
        else
          s""" "$value" """
        s"""     "${fieldName(field)}" : $valueFormatted"""
      }).mkString(",\n")

      s"""
         | "$paramName" : {
         |$v
         |     }
         |""".stripMargin
    }
    "{" + x.mkString(",\n") +
      """
        |,
        |"Ignore" : { "name" : "Ignore", "description" : "ignore this field", "type" : "string", "required" : false, "default" : "this is a dummy field and is ignored" }
      """.stripMargin +
      "}"
  }

  def schemaSource(name: String): NodeSchemaSource.EnumVal = {
    import scala.reflect.runtime.{universe => ru}

    val mirror = ru.runtimeMirror(getClass.getClassLoader)
    val moduleSymbol = mirror.staticModule(name)
    val moduleMirror = mirror.reflectModule(moduleSymbol)
    val instanceMirror = mirror.reflect(moduleMirror.instance)
    val field = moduleSymbol.typeSignature.member(ru.TermName("schemaSource"))

    instanceMirror.reflectField(field.asTerm).get.asInstanceOf[NodeSchemaSource.EnumVal]
  }

  def fixedSchema(name: String): JsObject = {
    import scala.reflect.runtime.{universe => ru}

    val mirror = ru.runtimeMirror(getClass.getClassLoader)
    val moduleSymbol = mirror.staticModule(name)
    val moduleMirror = mirror.reflectModule(moduleSymbol)
    val instanceMirror = mirror.reflect(moduleMirror.instance)
    val field = moduleSymbol.typeSignature.member(ru.TermName("fixedSchema"))

    val schema = instanceMirror.reflectField(field.asTerm).get.asInstanceOf[Option[JsObject]].get

    Json.obj(
      "widget-type" -> "non-editable-schema-editor",
      "schema" -> schema
    )
  }

  def nodeParamToWidgetJson(name: String): JsObject = {
    import scala.reflect.runtime.{universe => ru}

    val fieldName = Map(
      "name" -> "name",
      "description" -> "label",
      "typeString" -> "type",
      "required" -> "required",
      "defaultJsonString" -> "default")

    val tpeToWidgetType = Map(
      "boolean" -> "select",
      "string" -> "textbox",
      "int" -> "number",
      "seqOfString" -> "csv"
    )

    val tpeToWidgetAttr = Map(
      "boolean" -> Json.obj(
        "widget-attributes" -> Json.obj(
          "values" -> Json.arr(
            "true", "false"
          )
        )
      )
    )

    val runtimeMirror = ru.runtimeMirror(getClass.getClassLoader)
    val classSym = runtimeMirror.classSymbol(Class.forName(s"$name$$Params"))
    val paramTpes = classSym.selfType.decls.collect {
      case method: ru.MethodSymbol if method.isCaseAccessor => method.typeSignature
    }

    val properties: Iterable[JsObject] = for (paramTpe <- paramTpes) yield {
      val cm = runtimeMirror.reflectClass(paramTpe.typeSymbol.asClass)
      val ctor = paramTpe.decl(ru.termNames.CONSTRUCTOR).asMethod
      val ctorm = cm.reflectConstructor(ctor)
      val ctorParamTpe = ctor.typeSignature.paramLists.head.head.typeSignature
      val paramInst = if (ctorParamTpe =:= ru.typeOf[String])
        ctorm("x") //if string, boolean etc
      else if (ctorParamTpe =:= ru.typeOf[Boolean])
        ctorm(false)
      else if (ctorParamTpe =:= ru.typeOf[Integer])
        ctorm(0)
      else if (ctorParamTpe =:= ru.typeOf[Seq[String]])
        ctorm(List("x"))
      else
        throw new Exception(s"Not supported $ctorParamTpe")

      val configTermSym = paramTpe.member(ru.TermName("config"))
      val config = runtimeMirror.reflect(paramInst).reflectField(configTermSym.asTerm).get

      var paramName = "dummy"
      var paramRequired = true
      val fieldMap = (for (field <- List("name", "description", "typeString", "required", "defaultJsonString")) yield {
        val sym = runtimeMirror.reflect(config).symbol.asType.toType.member(ru.TermName(field))
        val value = runtimeMirror.reflect(config).reflectField(sym.asTerm).get
        paramName = if (field == "name") value.toString else paramName
        paramRequired = field == "required" && value == true
        val valueFormatted = if (value == true || value == false || field == "defaultJsonString")
          value.toString
        else
          s""" "$value" """

        fieldName(field) -> value.toString
      }).toMap

      val jsonObj = Json.obj(
        "widget-type" -> tpeToWidgetType(fieldMap("type")),
        "label" -> fieldMap("label"),
        "name" -> fieldMap("name")
      )
      if (tpeToWidgetAttr.contains(fieldMap("type"))) {
        var widgetAttr = tpeToWidgetAttr(fieldMap("type"))
        val default = fieldMap("default") //FIXME: should go "inside"
        if (default != "") widgetAttr = widgetAttr.deepMerge(Json.obj("default" -> JsString(default)))
        jsonObj ++ widgetAttr
      } else {
        jsonObj
      }
    }

    val schemaSourceType = schemaSource(name)

    val outputSchema = schemaSourceType match {
      case NodeSchemaSource.Fixed => fixedSchema(name)
      case NodeSchemaSource.Calculated => Json.obj()
      case NodeSchemaSource.User =>
        Json.obj(
          "name" -> "schema",
          "widget-type" -> "schema",
          "widget-attributes" -> Json.obj(
            "schema-types" -> List(
              "boolean",
              "int",
              "long",
              "float",
              "double",
              "string",
              "map<string, string>"
            ),
            "schema-default-type" -> "string"
          )
        )

    }

    val paramsConfGroup = Json.obj(
      "label" -> s"Configuration for $name",
      "properties" -> properties
    )

    val schemaGetterConfGroup = Json.obj(
      "label" -> s"Schema Updater",
      "properties" ->
        Json.arr(
          Json.obj(
            "widget-type" -> "textbox",
            "label" -> "Ignore",
            "name" -> "Ignore",
            "plugin-function" ->
              Json.obj(
                "method" -> "POST",
                "widget" -> "outputSchema",
                "output-property" -> "schema",
                "plugin-method" -> "outputSchema"
              )
          )
        )
    )

    val confGroups = if (schemaSourceType == NodeSchemaSource.Calculated)
      List(paramsConfGroup, schemaGetterConfGroup)
    else
      List(paramsConfGroup)

    Json.obj(
      "metadata" -> Json.obj("spec-version" -> "1.0"),
      "configuration-groups" -> confGroups,
      "outputs" -> List(outputSchema)
    )
  }

  lazy val nodes: Seq[NodeType] = nodeNames.map(nameToNodeType)

  lazy val nodeTypeDAO: BaseDAO[NodeTypesTable, NodeType] = new SimpleDAO[NodeTypesTable, NodeType] {
    override protected val tableQ: dbConfig.driver.api.TableQuery[NodeTypesTable] = SlickTables.nodeTypesTable
  }

  /**
    * insert the node types into the DB from where REST APIs can retrieve them
    */
  def populate(): Unit = {
    nodeTypeDAO.insert(nodes)
  }
}
