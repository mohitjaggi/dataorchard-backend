/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import play.api._


object Global extends GlobalSettings {

  override def onStart(app: Application) {
    NodeDB.populate()
  }

}
