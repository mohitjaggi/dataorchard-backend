/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import org.scalatestplus.play._
import play.api.test.Helpers.{contentType, _}
import play.api.test._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  * For more information, consult the wiki.
  */

class ApplicationSpec extends PlaySpec with OneAppPerTest {

  "Routes" should {

    "send 307 on a unhandled GET request" in {
      route(app, FakeRequest(GET, "/boum")).map(status(_)) mustBe Some(TEMPORARY_REDIRECT)
    }

    "send 307 on a unhandled PUT request" in {
      route(app, FakeRequest(PUT, "/boum")).map(status(_)) mustBe Some(TEMPORARY_REDIRECT)
    }

    "send 307 on a unhandled POST request" in {
      route(app, FakeRequest(POST, "/boum")).map(status(_)) mustBe Some(TEMPORARY_REDIRECT)
    }

    "send 307 on a unhandled DELETE request" in {
      route(app, FakeRequest(DELETE, "/boum")).map(status(_)) mustBe Some(TEMPORARY_REDIRECT)
    }

    "send yo on index" in {
      route(app, FakeRequest(GET, "/")).map(contentAsString(_)) contains "<title>yo<title>"
    }

    "GET uri apps all" in {
      route(app, FakeRequest(GET, "/v3/namespaces/default/apps")).map(status(_)) mustBe Some(OK)
    }

    "GET uri apps name" in {
      route(app, FakeRequest(GET, "/v3/namespaces/default/apps/someApp")).map(status(_)) mustBe Some(NO_CONTENT)
    }
  }
}
