/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import com.google.inject.AbstractModule
import controllers.routes
import net.codingwell.scalaguice.ScalaModule
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.{OneAppPerSuite, PlaySpec}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.FakeRequest
import play.api.test.Helpers.{contentAsString, contentType, _}

/**
  * Test case for the [[controllers.ApplicationController]] class.
  */
class SchemaControllerSpec extends PlaySpec with MockitoSugar with OneAppPerSuite {
  implicit override lazy val app = new GuiceApplicationBuilder()
    .overrides(new FakeModule)
    .build()

  class FakeModule extends AbstractModule with ScalaModule {
    def configure() = {
    }
  }

  "The `view` action with cookie env" should {
    "serve the create schema form" in {

      val Some(result) = route(app, FakeRequest(routes.SchemaController.view()))

      status(result) mustBe OK
      contentType(result) mustBe Some("text/html")
      contentAsString(result) must include("<title>Create Schema</title>")
    }
  }

}
