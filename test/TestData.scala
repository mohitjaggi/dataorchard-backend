import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import models.entities.{Group, User, UserGroup}

/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

object TestData {
  val appPutJson =
    """
      |{"artifact":{"id":0,"name":"cdap-data-pipeline","version":"3.5.1","scope":"SYSTEM","label":"Data Pipeline - Batch"},"__ui__":{"nodes":[{"plugin":{"label":"TextFileReader_label","artifact":{"id":0,"name":"cdap-data-pipeline","version":"3.5.1","scope":"SYSTEM"},"name":"TextFileReader","properties":{"deleteOnSuccess":"false","file":"/tmp/x"}},"icon":"fa-plug","type":"batchsource","warning":true,"_uiPosition":{"top":"150px","left":"144px"},"name":"TextFileReader_label","errorCount":0,"_backendProperties":{"file":{"name":"fileName","description":"name of input file","type":"string","required":true},"deleteOnSuccess":{"name":"deleteOnSuccess","description":"delete file when done","type":"boolean","required":false,"default":false}},"implicitSchema":{"line":"string"},"outputSchemaProperty":null,"outputSchema":"{\"type\":\"record\",\"name\":\"etlSchemaBody\",\"fields\":[{\"name\":\"line\",\"type\":\"string\"}]}","selected":false},{"plugin":{"label":"WordCounter","artifact":{"id":0,"name":"cdap-data-pipeline","version":"3.5.1","scope":"SYSTEM"},"name":"WordCounter","properties":{"delimiter":" "}},"icon":"fa-plug","type":"batchaggregator","warning":true,"_uiPosition":{"top":"150px","left":"432px"},"name":"WordCounter","errorCount":0,"_backendProperties":{"delimiter":{"name":"delimiter","description":"split on this to get words from text","type":"string","required":false,"default":" "}},"implicitSchema":{"word":"string","count":"integer"},"outputSchemaProperty":null,"outputSchema":"{\"name\":\"etlSchemaBody\",\"type\":\"record\",\"fields\":[{\"name\":\"word\",\"type\":\"string\"},{\"name\":\"count\",\"type\":\"integer\"}]}","selected":false,"inputSchema":[{"name":"TextFileReader_label","schema":"{\"type\":\"record\",\"name\":\"etlSchemaBody\",\"fields\":[{\"name\":\"line\",\"type\":\"string\"}]}"}]},{"plugin":{"label":"CsvFileWriter","artifact":{"id":0,"name":"cdap-data-pipeline","version":"3.5.1","scope":"SYSTEM"},"name":"CsvFileWriter","properties":{"coalesce":"true","delimiter":",","file":"/tmp/wc"}},"icon":"fa-plug","type":"batchsink","warning":true,"_uiPosition":{"top":"150px","left":"720px"},"name":"CsvFileWriter","errorCount":0,"_backendProperties":{"file":{"name":"fileName","description":"name of output file","type":"string","required":true},"coalesce":{"name":"singlePart","description":"merge output to single partition","type":"string","required":false,"default":true},"delimiter":{"name":"delimiter","description":"use this to separate fields","type":"string","required":false,"default":","}},"implicitSchema":{"word":"string","count":"integer"},"outputSchemaProperty":null,"outputSchema":"{\"name\":\"etlSchemaBody\",\"type\":\"record\",\"fields\":[{\"name\":\"word\",\"type\":\"string\"},{\"name\":\"count\",\"type\":\"integer\"}]}","selected":false,"inputSchema":[{"name":"WordCounter","schema":"{\"name\":\"etlSchemaBody\",\"type\":\"record\",\"fields\":[{\"name\":\"word\",\"type\":\"string\"},{\"name\":\"count\",\"type\":\"integer\"}]}"}]}],"draftId":"a04e07f3-4321-4c6b-889a-32651a481e55"},"description":"word count 1","name":"wc1","config":{"resources":{"memoryMB":512,"virtualCores":1},"driverResources":{"memoryMB":512,"virtualCores":1},"connections":[{"from":"TextFileReader_label","to":"WordCounter"},{"from":"WordCounter","to":"CsvFileWriter"}],"comments":[],"postActions":[],"stages":[{"name":"TextFileReader_label","plugin":{"name":"TextFileReader","type":"batchsource","label":"TextFileReader_label","artifact":{"id":0,"name":"cdap-data-pipeline","version":"3.5.1","scope":"SYSTEM"},"properties":{"deleteOnSuccess":"false","file":"/tmp/x"}},"outputSchema":"{\"type\":\"record\",\"name\":\"etlSchemaBody\",\"fields\":[{\"name\":\"line\",\"type\":\"string\"}]}"},{"name":"WordCounter","plugin":{"name":"WordCounter","type":"batchaggregator","label":"WordCounter","artifact":{"id":0,"name":"cdap-data-pipeline","version":"3.5.1","scope":"SYSTEM"},"properties":{"delimiter":" "}},"outputSchema":"{\"name\":\"etlSchemaBody\",\"type\":\"record\",\"fields\":[{\"name\":\"word\",\"type\":\"string\"},{\"name\":\"count\",\"type\":\"integer\"}]}","inputSchema":[{"name":"TextFileReader_label","schema":"{\"type\":\"record\",\"name\":\"etlSchemaBody\",\"fields\":[{\"name\":\"line\",\"type\":\"string\"}]}"}]},{"name":"CsvFileWriter","plugin":{"name":"CsvFileWriter","type":"batchsink","label":"CsvFileWriter","artifact":{"id":0,"name":"cdap-data-pipeline","version":"3.5.1","scope":"SYSTEM"},"properties":{"coalesce":"true","delimiter":",","file":"/tmp/wc"}},"outputSchema":"{\"name\":\"etlSchemaBody\",\"type\":\"record\",\"fields\":[{\"name\":\"word\",\"type\":\"string\"},{\"name\":\"count\",\"type\":\"integer\"}]}","inputSchema":[{"name":"WordCounter","schema":"{\"name\":\"etlSchemaBody\",\"type\":\"record\",\"fields\":[{\"name\":\"word\",\"type\":\"string\"},{\"name\":\"count\",\"type\":\"integer\"}]}"}]}],"schedule":"0 * * * *","engine":"mapreduce","description":"word count 1"}}
    """.stripMargin

  val userPostJson =
    """
      |{
      |    "userId" : "mohit@gmail.com",
      |    "firstName" : "Mohit",
      |    "lastName" : "Jaggi",
      |    "description" : "hey it's me!"
      |}
    """.stripMargin

  val defaultUser1 = User.fromExternal(
    uuid = UUID.fromString(User.defaultUserId1),
    loginInfo = LoginInfo("credentials", "test@gmail.com"),
    firstName = Some("test"),
    lastName = Some("user"),
    email = Some("test@gmail.com"),
    avatarURL = None,
    confirmed = true
  )

  val defaultUser2 = User.fromExternal(
    uuid = UUID.fromString(User.defaultUserId2),
    loginInfo = LoginInfo("basic-auth", "test@gmail.com"),
    firstName = Some("test"),
    lastName = Some("user"),
    email = Some("test@gmail.com"),
    avatarURL = None,
    confirmed = true
  )

  val testGroup1 = Group(1, "grp1", "first test group")

  val testGroup2 = Group(2, "grp2", "second test group")

  val testUserGroup1 = UserGroup(User.defaultUserId1, 0)

  val testUserGroup2 = UserGroup(User.defaultUserId2, 0)

}
