/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import java.util.UUID

import com.google.inject.AbstractModule
import com.mohiva.play.silhouette.api.{Environment, LoginInfo}
import com.mohiva.play.silhouette.test._
import models.entities.User
import net.codingwell.scalaguice.ScalaModule
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.{OneAppPerSuite, PlaySpec}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.concurrent.Execution.Implicits._
import play.api.test.FakeRequest
import play.api.test.Helpers.{contentAsString, contentType, _}
import utils.auth.{BearerTokenEnv, CookieEnv}
import controllers.routes

/**
  * Test case for the [[controllers.ApplicationController]] class.
  */
class ApplicationControllerSpec extends PlaySpec with MockitoSugar with OneAppPerSuite {
  implicit override lazy val app = new GuiceApplicationBuilder()
    .overrides(new FakeModule)
    .build()

  class FakeModule extends AbstractModule with ScalaModule {
    def configure() = {
      bind[Environment[CookieEnv]].toInstance(cookieEnv)
      bind[Environment[BearerTokenEnv]].toInstance(tokenEnv)
    }
  }

  val identity = User.fromExternal(
    uuid = UUID.randomUUID(),
    loginInfo = LoginInfo("facebook", "user@facebook.com"),
    firstName = None,
    lastName = None,
    email = None,
    avatarURL = None,
    confirmed = true
  )

  /**
    * A Silhouette fake environment.
    */
  implicit val cookieEnv: Environment[CookieEnv] = new FakeEnvironment[CookieEnv](Seq(identity.loginInfo -> identity))
  implicit val tokenEnv: Environment[BearerTokenEnv] = new FakeEnvironment[BearerTokenEnv](Seq(identity.loginInfo -> identity))

  "The `index` action with cookie env" should {
    "redirect to login page if user is unauthorized" in {

      val Some(redirectResult) = route(app, FakeRequest(routes.ApplicationController.index())
        .withAuthenticator[CookieEnv](LoginInfo("invalid", "invalid"))
      )

      status(redirectResult) mustBe SEE_OTHER

      val redirectURL = redirectLocation(redirectResult).getOrElse("")
      redirectURL must include(routes.SignInController.view().toString)

      val Some(unauthorizedResult) = route(app, FakeRequest(GET, redirectURL))

      status(unauthorizedResult) mustBe OK
      contentType(unauthorizedResult) mustBe Some("text/html")
      contentAsString(unauthorizedResult) must include("<title>DataGears - Sign In</title>")
    }

    "return 200 if user is authorized" in {
      val Some(result) = route(app, FakeRequest(routes.ApplicationController.index())
        .withAuthenticator[CookieEnv](identity.loginInfo)
      )

      status(result) mustBe OK
    }
  }

  "The `index` action with bearer token env" should {
    "redirect to login page if user is unauthorized" in {

      val Some(redirectResult) = route(app, FakeRequest(routes.BearerApplicationController.index())
        .withAuthenticator[BearerTokenEnv](LoginInfo("invalid", "invalid"))
      )

      status(redirectResult) mustBe SEE_OTHER

      val redirectURL = redirectLocation(redirectResult).getOrElse("")
      redirectURL must include(routes.SignInController.view().toString)

      val Some(unauthorizedResult) = route(app, FakeRequest(GET, redirectURL))

      status(unauthorizedResult) mustBe OK
      contentType(unauthorizedResult) mustBe Some("text/html")
      contentAsString(unauthorizedResult) must include("<title>DataGears - Sign In</title>")
    }

    "return 200 if user is authorized" in {
      val Some(result) = route(app, FakeRequest(routes.BearerApplicationController.index())
        .withAuthenticator[BearerTokenEnv](identity.loginInfo)
      )

      status(result) mustBe OK
    }

    "fetch bearer token" in {
      val Some(result) = route(app, FakeRequest(routes.BearerApplicationController.getToken())
        .withAuthenticator[BearerTokenEnv](identity.loginInfo)
      )

      status(result) mustBe OK
      contentType(result) mustBe Some("application/json")
      contentAsString(result) must include("token_type")
    }
  }
}
