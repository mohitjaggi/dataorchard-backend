/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */


import org.scalatestplus.play._
import play.api.libs.json._
import play.api.test.Helpers._
import play.api.test._

class NodeDBSpec extends PlaySpec with OneAppPerSuite {

  "NodeDB REST API" should {
    "GET supported nodetypes" in {
      NodeDB.populate()
      val response = route(app, FakeRequest(GET, "/plugin")).map(contentAsJson(_)).get
      response.as[JsArray].value.map { v => "dag.nodetypes." + (v \ "name").as[String] }.toSet mustBe dag.nodetypes.nodeNames.toSet
    }

    "GET widget json" in {
      for(node <- NodeDB.nodes) {
        val response = route(app, FakeRequest(GET,
          s"/v3/namespaces/default/artifacts/cdap-data-pipeline/versions/3.5.1/properties?keys=widgets.${node.name}-${node.tpe}&scope=SYSTEM"))
          .map(contentAsJson(_))
        response must not be (None)
        val responseValue = (response.get \ s"widgets.${node.name}-${node.tpe}")
       // (responseValue \ "metadata" \ "spec-version").as[String] mustBe "1.0"
        //((responseValue \ "configuration-groups").head \ "label").as[String] must endWith (node.name)
      }
    }
  }
}
