/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import controllers.JsonSerDes._
import models.entities.{Group, User, UserGroup, UserGroupJoined}
import org.scalatestplus.play._
import play.api.libs.json.{JsResult, JsSuccess, Json}
import play.api.test.Helpers.{contentAsJson, contentType, _}
import play.api.test._

class UserGroupSpec extends PlaySpec with OneAppPerTest {

  "REST API" should {
    "PUT user and GET it back" in {
      // nothing there
      val Some(response) = route(app, FakeRequest(GET, "/admin/users"))
      status(response) mustBe OK
      contentType(response) mustBe Some("application/json")
      val json = contentAsJson(response)
      (json \\ "userIdStr").isEmpty mustBe true

      // put a user
      route(app, FakeRequest(GET, "/admin/test/init",
        FakeHeaders(List((CONTENT_TYPE, TEXT))), "init")).map(status(_)) mustBe Some(OK)

      // get it back
      val Some(response1) = route(app, FakeRequest(GET, "/admin/users"))
      status(response1) mustBe OK
      contentType(response1) mustBe Some("application/json")
      val json1 = contentAsJson(response1)
      (json1(0) \ "userIDStr").as[String] mustBe User.defaultUserId1
      json1(0).as[User] mustBe TestData.defaultUser1
      json1.as[Seq[User]].length mustBe 1

      // put another user
      route(app, FakeRequest(GET, "/admin/test/init2",
        FakeHeaders(List((CONTENT_TYPE, TEXT))), "init2")).map(status(_)) mustBe Some(OK)

      // get it back
      val Some(response2) = route(app, FakeRequest(GET, "/admin/users"))
      status(response2) mustBe OK
      contentType(response2) mustBe Some("application/json")
      val json2 = contentAsJson(response2)
      json2.as[Seq[User]].toSet mustBe Set(TestData.defaultUser1, TestData.defaultUser2)
    }

    "PUT group and GET it back" in {
      // nothing there except "default" group
      val Some(response) = route(app, FakeRequest(GET, "/admin/groups"))
      status(response) mustBe OK
      contentType(response) mustBe Some("application/json")
      val json = contentAsJson(response)
      json.as[Seq[Group]].length mustBe 1

      // put a group
      route(app, FakeRequest(PUT, "/admin/groups",
        FakeHeaders(List((CONTENT_TYPE, JSON))), Json.toJson(TestData.testGroup1))).map(status(_)) mustBe Some(OK)

      // now we have 2 groups
      val Some(response1) = route(app, FakeRequest(GET, "/admin/groups"))
      status(response1) mustBe OK
      contentType(response1) mustBe Some("application/json")
      val json1 = contentAsJson(response1)
      json1.as[Seq[Group]].length mustBe 2

      // put another group
      route(app, FakeRequest(PUT, "/admin/groups",
        FakeHeaders(List((CONTENT_TYPE, JSON))), Json.toJson(TestData.testGroup2))).map(status(_)) mustBe Some(OK)

      // get it back, filter out "default" and we should have what we added
      val Some(response2) = route(app, FakeRequest(GET, "/admin/groups"))
      status(response2) mustBe OK
      contentType(response2) mustBe Some("application/json")
      val json2 = contentAsJson(response2)
      json2.as[Seq[Group]].toSet.filter(_.name != "default") mustBe Set(TestData.testGroup1, TestData.testGroup2)
    }

    "PUT user group and GET it back" in {
      // nothing there
      val Some(response) = route(app, FakeRequest(GET, "/admin/userGroups"))
      status(response) mustBe OK
      contentType(response) mustBe Some("application/json")
      val json = contentAsJson(response)
      json.as[Seq[UserGroup]].length mustBe 0

      // put a user
      route(app, FakeRequest(GET, "/admin/test/init",
        FakeHeaders(List((CONTENT_TYPE, TEXT))), "init")).map(status(_)) mustBe Some(OK)

      // put a user group
      route(app, FakeRequest(PUT, "/admin/userGroups",
        FakeHeaders(List((CONTENT_TYPE, JSON))), Json.toJson(TestData.testUserGroup1))).map(status(_)) mustBe Some(OK)

      // get it back, filter out "default" and we should have what we added
      val Some(response2) = route(app, FakeRequest(GET, "/admin/userGroups"))
      status(response2) mustBe OK
      contentType(response2) mustBe Some("application/json")
      val json2 = contentAsJson(response2)
      json2.as[Seq[UserGroup]].toSet mustBe Set(TestData.testUserGroup1)

      val Some(response3) = route(app, FakeRequest(GET, "/admin/userGroupsDetail"))
      status(response3) mustBe OK
      contentType(response3) mustBe Some("application/json")
      val json3 = contentAsJson(response3)
      json3.as[Seq[UserGroupJoined]].length mustBe 1
    }
  }
}
