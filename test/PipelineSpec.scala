/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

import org.scalatestplus.play._
import play.api.test.Helpers.{contentType, _}
import play.api.test._

class PipelineSpec extends PlaySpec with OneAppPerTest {

  "REST API" should {
    "PUT pipeline name and GET it back" in {
      // nothing there
      route(app, FakeRequest(GET, "/v3/namespaces/default/apps/wc1")).map(status(_)) mustBe Some(NO_CONTENT)
      for(uri <- List ("/pipelines", "/pipelines/wc1", "/admin/pipelines", "/admin/pipelines/wc1")) {
        val Some(response) = route(app, FakeRequest(GET, uri))
        status(response) mustBe OK
        contentType(response) mustBe Some("application/json")
        val json = contentAsJson(response)
        (json \\ "frontendId").isEmpty mustBe true
      }

      // put something
      route(app, FakeRequest(PUT, "/v3/namespaces/default/apps/wc1",
        FakeHeaders(List((CONTENT_TYPE, JSON))), TestData.appPutJson)).map(status(_)) mustBe Some(OK)

      // get it back
      route(app, FakeRequest(GET, "/v3/namespaces/default/apps/wc1")).map(status(_)) mustBe Some(OK) //content won't match
      route(app, FakeRequest(GET, "/pipelines/wc1")).map(status(_)) mustBe Some(OK) //content won't match

      for(uri <- List ("/pipelines", "/pipelines/wc1", "/admin/pipelines", "/admin/pipelines/wc1")) {
        val Some(response) = route(app, FakeRequest(GET, uri))
        status(response) mustBe OK
        contentType(response) mustBe Some("application/json")
        val json = contentAsJson(response)
        (json \\ "frontendId").isEmpty mustBe false
      }
    }
  }
}
