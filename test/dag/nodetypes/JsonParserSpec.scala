/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

package dag.nodetypes

import macros.NodeTypeMacros
import org.scalatest.FunSuite
import play.api.libs.json._
import JsonWriters._

class JsonParserSpec extends FunSuite {
  test("TextFileReader to JSON and back") {
    val nodeIn = TextFileReader("input", TextFileReader.Params(
      CommonParams.FileName("/tmp/x"),
      CommonParams.DeleteOnSuccess(false)))
    val json = Json.toJson(nodeIn)
    val nodeOut = NodeTypeMacros.parse(json).asInstanceOf[TextFileReader]

    assert(nodeOut === nodeIn)
  }

  test("JdbcReader to JSON and back") {
    val nodeIn = JdbcReader("input", JdbcReader.Params(
      CommonParams.JdbcUrl("jdbc:hello:database"),
      CommonParams.JdbcTableName("MyTable"),
      CommonParams.UserName("admin"),
      CommonParams.Password("admin123")))
    val json = Json.toJson(nodeIn)
    val nodeOut = NodeTypeMacros.parse(json).asInstanceOf[JdbcReader]

    assert(nodeOut === nodeIn)
  }

  test("ParquetReader to JSON and back") {
    val nodeIn = ParquetReader("input", ParquetReader.Params(
      CommonParams.FileName("/tmp/x"),
      CommonParams.DeleteOnSuccess(false)))
    val json = Json.toJson(nodeIn)
    val nodeOut = NodeTypeMacros.parse(json).asInstanceOf[ParquetReader]

    assert(nodeOut === nodeIn)
  }

  test("ColumnSelector to JSON and back") {
    val nodeIn = ColumnSelector("input", ColumnSelector.Params(
      CommonParams.ColumnNames(List("col1", "col2"))))
    val json = Json.toJson(nodeIn)
    val nodeOut = NodeTypeMacros.parse(json).asInstanceOf[ColumnSelector]

    assert(nodeOut === nodeIn)
  }
}
