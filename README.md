(C) Copyright 2016, Data Orchard LLC
All rights reserved

All code in this repository is licensed under Apache v2 [license](https://www.apache.org/licenses/LICENSE-2.0 ).

# Data Visual.ly

## Command line setup (required)
`git clone <this repo>`

`sbt run` 

This downloads dependencies, compiles and starts server on port 9000

## test

`sbt test`

All tests should pass.

## Activator setup (recommended)
Download latest activator from Lightbend's website.
The activator scripts are checked in to the repo but the activator-launch-1.3.7.jar is not. Put that in the root directory of the repo (where this readme is)

`./activator ui` 

This starts activator server on an open port and open browser where you can edit code, build, run

## Set up IDE (recommended)
I use IntelliJ for editing and don't open files in activator to avoid write conflict on the file. But I use activator to run the server.

Intellij IDEA with Scala plugin -> import -> as SBT project
