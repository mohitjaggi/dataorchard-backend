-- Copyright (C) 2016
-- Data Orchard LLC
-- All rights reserved


# --- !Ups

create table "users" ("userId" VARCHAR(254) NOT NULL PRIMARY KEY,
    "providerId" VARCHAR(254) NOT NULL,
    "providerKey" VARCHAR(254) NOT NULL,
    "firstName" VARCHAR(254) NOT NULL,
    "lastName" VARCHAR(254) NOT NULL,
    "email" VARCHAR(254) NOT NULL,
    "avatarURL" VARCHAR(1024),
    "activated" BOOLEAN);

create table "groups" ("id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(254) NOT NULL UNIQUE,
    "description" text);

insert into "groups"
    values(0, 'default', 'default group for dev testing');

create table "users_groups" ("userId" VARCHAR(254) NOT NULL,
    "groupId" BIGINT NOT NULL,
     PRIMARY KEY ("userId", "groupId"));

create table "authTokens" ("id" VARCHAR(254) NOT NULL PRIMARY KEY,
    "userId" VARCHAR(254) NOT NULL,
    "expiry" VARCHAR(254) NOT NULL);

create table "artifacts"("id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(254) NOT NULL,
    "version" VARCHAR(16) NOT NULL,
    "scope" VARCHAR(16) NOT NULL);

insert into "artifacts"
    values (0, 'cdap-data-pipeline', '3.5.1', 'SYSTEM');

insert into "artifacts"
    values (1, 'DGNNodes', '1.0', 'SYSTEM');

create table "nodetypes" ("id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(254) NOT NULL UNIQUE,
    "description" VARCHAR(1023),
    "type" VARCHAR(254),
    "className" VARCHAR(512),
    "properties" VARCHAR(4096),
    "widget" VARCHAR(4096),
    "endPoints" VARCHAR(16),
    "artifact" VARCHAR(1024));

--insert into "nodetypes"
--    values(0, 'TextFileReader', 'read text files', 'batchsource', 'dummy',
--    '{
--          "fileName" : { "name" : "fileName", "description" : "name of input file", "type" : "string", "required" : true },
--          "deleteOnSuccess" : { "name" : "deleteOnSuccess", "description" : "delete file when done", "type" : "boolean", "required" : false, "default" : false }
--
--    }',
--    '{
--        "metadata":{
--           "spec-version":"1.0"
--        },
--        "configuration-groups":[
--           {
--              "label":"Text File Reader Configuration",
--              "properties":[
--                 {
--                    "widget-type":"dataset-selector",
--                    "label":"File Name",
--                    "name":"fileName"
--                 },
--                 {
--                    "widget-type":"select",
--                    "label":"Delete input file when pipeline finishes successfully",
--                    "name":"deleteOnSuccess",
--                    "widget-attributes":{
--                       "values":[
--                          "true",
--                          "false"
--                       ],
--                       "default":"false"
--                    }
--                 }
--              ]
--           }
--        ],
--        "outputs":[
--           {
--              "widget-type":"non-editable-schema-editor",
--              "schema":{
--                 "line":"string"
--              }
--           }
--        ]
--     }',
--    '[ ]', 'cdap-data-pipeline');
--
--insert into "nodetypes"
--    values(1, 'WordCounter', 'count words after splitting by delimiter', 'batchaggregator', 'dummy',
--    '{
--            "delimiter" : { "name" : "delimiter", "description" : "split on this to get words from text", "type" : "string", "required" : false, "default" : " " }
--    }',
--    '{
--             "metadata":{
--                "spec-version":"1.0"
--             },
--             "configuration-groups":[
--                {
--                   "label":"Word Counter Configuration",
--                   "properties":[
--                      {
--                         "widget-type":"textbox",
--                         "label":"Split text on",
--                         "name":"delimiter"
--                      }
--                   ]
--                }
--             ],
--             "outputs":[
--                {
--                   "widget-type":"non-editable-schema-editor",
--                   "schema":{
--                      "word":"string",
--                      "count":"int"
--                   }
--                }
--             ]
--          }',
--    '[ ]', 'cdap-data-pipeline');
--
--insert into "nodetypes"
--    values(2, 'CsvFileWriter', 'write to a CSV file', 'batchsink', 'dummy',
--    '{
--            "file" : { "name" : "fileName", "description" : "name of output file", "type" : "string", "required" : true },
--            "coalesce" : { "name" : "singlePart", "description" : "merge output to single partition", "type" : "string", "required" : false, "default" : true },
--            "delimiter" : { "name" : "delimiter", "description" : "use this to separate fields", "type" : "string", "required" : false, "default" : "," }
--    }',
--    '{
--             "metadata":{
--                "spec-version":"1.0"
--             },
--             "configuration-groups":[
--                {
--                   "label":"CSV File Writer Configuration",
--                   "properties":[
--                      {
--                        "widget-type":"textbox",
--                        "label":"Field separator",
--                        "name":"delimiter"
--                      },
--                      {
--                         "widget-type":"dataset-selector",
--                         "label":"File Name",
--                         "name":"file"
--                      },
--                      {
--                         "widget-type":"select",
--                         "label":"Combine to single partition",
--                         "name":"coalesce",
--                         "widget-attributes":{
--                            "values":[
--                               "true",
--                               "false"
--                            ],
--                            "default":"true"
--                         }
--                      }
--                   ]
--                }
--             ],
--             "outputs":[
--                {
--                   "widget-type":"non-editable-schema-editor",
--                   "schema":{
--                      "word":"string",
--                      "count":"int"
--                   }
--                }
--             ]
--          }',
--    '[ ]', 'cdap-data-pipeline');

--create table "pipelineStatus" ("id" INTEGER NOT NULL PRIMARY KEY,
--    "name" : VARCHAR(16));

--insert into "pipelineStatus"
--    values(1, "draft");
--insert into "pipelineStatus"
--    values(2, "published");

create table "pipelines" ("frontendId" VARCHAR(96) NOT NULL PRIMARY  KEY,
    "name" VARCHAR(256) NOT NULL,
    "description" VARCHAR(1024),
    "version" BIGINT NOT NULL,
    "status" VARCHAR(16),
    "jsonRepr" text,
    "createdBy" VARCHAR(254),
    "updatedBy" VARCHAR(254),
    "accessedBy" VARCHAR(254));
--    FOREIGN KEY ("status") REFERENCES pipelineStatus("id"));

create table "schemafields" ("schemaName" VARCHAR(128) NOT NULL,
    "schemaDescription" text,
    "fieldName" VARCHAR(128) NOT NULL,
    "fieldType" VARCHAR(96),
    "createdBy" VARCHAR(254),
    "updatedBy" VARCHAR(254),
    "accessedBy" VARCHAR(254),
    PRIMARY KEY ("schemaName", "fieldName", "createdBy"));

# --- !Downs
;

drop table "artifacts";
drop table "nodetypes";
drop table "pipelineStatus";
drop table "pipelines";
drop table "schemafields";
drop table "users_groups";
drop table "groups";
drop table "users";
