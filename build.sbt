/*
 * Copyright (c) 2016.
 * Data Orchard LLC
 * All rights reserved
 */

name := """datagears-webapp"""

version := "0.0.1"

lazy val webapp = project.in(file("."))
  .enablePlugins(PlayScala)
  .dependsOn(macros)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "2.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.0",
  "com.h2database" % "h2" % "1.4.191",
 // "org.postgresql" % "postgresql" % "9.4.1212",
  cache,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.0" % Test,
  "com.mohiva" %% "play-silhouette" % "4.0.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "4.0.0",
  "com.mohiva" %% "play-silhouette-persistence" % "4.0.0",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "4.0.0",
  "org.webjars" %% "webjars-play" % "2.5.0-2",
  "org.webjars" % "jquery-ui" % "1.12.1",
  "net.codingwell" %% "scala-guice" % "4.0.1",
  "com.iheart" %% "ficus" % "1.2.6",
  "com.typesafe.play" %% "play-mailer" % "5.0.0",
  "com.enragedginger" %% "akka-quartz-scheduler" % "1.5.0-akka-2.4.x",
  "com.adrianhurt" %% "play-bootstrap" % "1.0-P25-B3",
  "com.mohiva" %% "play-silhouette-testkit" % "4.0.0" % Test,
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.2.0",
  "joda-time" % "joda-time" % "2.7",
  "org.joda" % "joda-convert" % "1.7",
  "org.mockito" % "mockito-all" % "2.0.2-beta" % Test,
  //specs2 % Test,
  //"org.specs2" %% "specs2-core" % "3.8.5" % Test,
  filters
)

libraryDependencies += "org.scala-graph" %% "graph-core" % "1.11.2"

libraryDependencies += "org.clapper" %% "classutil" % "1.0.13"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

resolvers += Resolver.url("Typesafe Ivy releases", url("https://repo.typesafe.com/typesafe/ivy-releases"))(Resolver.ivyStylePatterns)

resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"

resolvers += Resolver.jcenterRepo

fork in run := true

lazy val macros = project in file("macros")

//scalacOptions += "-Ylog-classpath"
